<?php
    include('header.php');
    $coupon_id = "";
    if(isset($_REQUEST['id'])){
        $coupon_id = $_REQUEST['id'];
    }
    echo "<input type='hidden' value='".$coupon_id."' id='couponId' />";
?>
<div class="right_col" role="main">
    <div class="row tile_count">
        <a href="users"><div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                <span class="count_top"><i class="fa fa-user"></i> Total Users</span>
                <div class="count" id="userCount"></div>
                <span class="count_bottom"><i class="green">Click </i>to Expand</span>
            </div></a>
        <a href="books"><div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                <span class="count_top"><i class="fa fa-volume-up"></i> Total Audio Books</span>
                <div class="count" id="booksCount"></div>
                <span class="count_bottom"><i class="green"></i> in All Categories</span>
            </div></a>
        <a href="index"><div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                <span class="count_top"><i class="fa fa-book"></i> Total Categories</span>
                <div class="count green" id="catCount"></div>
                <span class="count_bottom"><i class="green"></i> Click to Expand</span>
            </div></a>
        <a href="membership"><div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                <span class="count_top"><i class="fa fa-users"></i> MemberShip Types</span>
                <div class="count" id="membershipCount"></div>
                <span class="count_bottom"> Click to View</span>
            </div></a>
        <a href="discount"><div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                <span class="count_top"><i class="fa fa-cc-discover"></i> Discount Coupons</span>
                <div class="count" id="allCoupon"></div>
                <span class="count_bottom"><i class="green" id="activeCoupon"></i> is Still Active</span>
            </div></a>
        <a href="orders"><div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                <span class="count_top"><i class="fa fa-first-order"></i> Orders</span>
                <div class="count" id="orderCount"></div>
                <span class="count_bottom"><i class="green"></i>Click to Expand</span>
            </div></a>
    </div>
    <div class="">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Generate New Coupon Code <small></small></h2>
                        <ul class="nav navbar-right panel_toolbox"></ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <p id='message' style='text-align: center;font-size: 14px;color: red'></p>
                        <div class='row'>
                            <label class='label label-danger'>Please Fill the Required Data</label>
                            <label class='error'></label>
                        </div>
                        <br>
                        <div class='row'>
                            <div class='col-md-4 form-group'>
                                <label>Coupon Code</label>
                                <input type='text' id='coupon_code' value='' placeholder='Enter Coupon Code' class='form-control' />
                            </div>
                            <div class='col-md-4 form-group'>
                                <label>Code Value</label>
                                <input type='text' class='form-control' id='code_value' value='' placeholder='Value Should Be In USD'/>
                            </div>
                        </div>
                        <div class='row'>
                            <div class='col-md-4 form-group'>
                                <label>Activation Date</label>
                                <input type='text' class='form-control' id='datetimepicker6' />
                            </div>
                            <div class='col-md-4 form-group'>
                                <label>Expiry Date</label>
                                <input type='text' class='form-control' id='datetimepicker7' />
                            </div>
                        </div>
                        <div class='row'>
                            <div class='col-md-4 form-group'>
                                <label>Select Visibilty Status</label>
                                <div>
                                    <label><input type='radio' name='coupon_status' id='on' checked value='1' /> Show </label>
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    <label>
                                        <input id='off' type='radio' name='coupon_status' value='0' /> Hide
                                    </label>
                                </div>
                            </div>
                            <?php if(isset($_REQUEST['id'])){
                                ?>
                                <input type='hidden' value='editCoupon' id='couponservicetype' />
                                <?php
                            }else{
                                ?>
                                <input type='hidden' value='addCoupon' id='couponservicetype' />
                                <?php
                            }
                            ?>

                            <div class='form-group col-md-2'></div>
                            <div class='form-group col-md-6' style='text-align:right; margin-top:40px'>
                                <input type='button' value='Cancel' data-dismiss='modal' class='btn btn-default'/>
                                <?php if(isset($_REQUEST['id'])){
                                    ?>
                                    <input type='button' value='Update Coupon Data' onclick=couponprocess('<?php echo $coupon_id ?>') class='btn btn-info formbtn' />
                                    <?php
                                }else{
                                    ?>
                                    <input type='button' value='Add New Coupon Code' onclick=couponprocess('') class='btn btn-info formbtn' />
                                    <?php
                                }
                                ?>
                            </div>
                        </div>
                        <img src='images/default.gif' class='loadNow' />
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
    include("footer.php");
?>
<script>
var couponId = $("#couponId").val();
if(couponId != ""){
    editDiscountCoupon(couponId);
}
</script>
