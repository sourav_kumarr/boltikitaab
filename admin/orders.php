<?php
include('header.php');
include('api/Classes/CONNECT.php');
include('api/Constants/DbConfig.php');
include('api/Constants/configuration.php');
require_once('api/Classes/USERCLASS.php');
$conn = new \Classes\CONNECT();
$userClass = new \Classes\USERCLASS();
if(isset($_POST['filterButton'])){
    $startDate = strtotime($_POST['startDate']);
    $endDate = strtotime($_POST['endDate']);
    $query="select * from orders where order_dated>='$startDate' and order_dated<='$endDate' order by order_id DESC";
}else{
    $query="select * from orders order by order_id DESC";
}
?>
<!-- page content -->
<div class="right_col" role="main">
    <div class="row tile_count">
        <a href="users"><div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                <span class="count_top"><i class="fa fa-user"></i> Total Users</span>
                <div class="count" id="userCount"></div>
                <span class="count_bottom"><i class="green">Click</i> to Expand</span>
            </div></a>
        <a href="books"><div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                <span class="count_top"><i class="fa fa-volume-up"></i> Total Audio Books</span>
                <div class="count" id="booksCount"></div>
                <span class="count_bottom"><i class="green"></i> in All Categories</span>
            </div></a>
        <a href="index"><div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                <span class="count_top"><i class="fa fa-book"></i> Total Categories</span>
                <div class="count green" id="catCount"></div>
                <span class="count_bottom"><i class="green"></i> Click to Expand</span>
            </div></a>
        <a href="membership"><div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                <span class="count_top"><i class="fa fa-users"></i> MemberShip Types</span>
                <div class="count" id="membershipCount"></div>
                <span class="count_bottom"> Click to View</span>
            </div></a>
        <a href="discount"><div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                <span class="count_top"><i class="fa fa-cc-discover"></i> Discount Coupons</span>
                <div class="count" id="allCoupon"></div>
                <span class="count_bottom"><i class="green" id="activeCoupon"></i> is Still Active</span>
            </div></a>
        <a href="orders"><div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                <span class="count_top"><i class="fa fa-first-order"></i> Orders</span>
                <div class="count" id="orderCount"></div>
                <span class="count_bottom"><i class="green"></i>Click to Expand</span>
            </div></a>
    </div>
    <div class="">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>All Orders <small></small></h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li>
                                <button style="margin-top:5px" onclick="window.location='api/excelProcess.php?dataType=allOrders'" class="btn btn-info btn-sm">Download Excel File</button>
                            </li>
                            <li>
                                <form method="post" class="form-inline">
                                    <div class="form-group form-inline">
                                        <input type="text" placeholder="Start Date" class="form-control" name="startDate" id="startFilter" />
                                    </div>
                                    <div class="form-group">
                                        <input type="text" placeholder="End Date" class="form-control" name="endDate" id="endFilter" />
                                    </div>
                                    <input type="submit" Value="Go" class="btn btn-warning btn-sm" name="filterButton" style="margin-top: 5px" />
                                </form>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <p class="text-muted font-13 m-b-30">
                            List of Orders Generated
                        </p>
                        <table id="orderTable" class="table table-striped table-bordered">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>User Name</th>
                                <th>Order Id</th>
                                <th>Transaction Id</th>
                                <th>Order Amount</th>
                                <th>Order Dated</th>
                                <th>Order Status</th>
                                <th>Payment Status</th>
                                <th>Refund Status</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $link = $conn->connect();
                            if ($link) {
                                $result = mysqli_query($link, $query);
                                if ($result) {
                                    $num = mysqli_num_rows($result);
                                    if ($num > 0) {
                                        $j = 0;
                                        while ($orderData = mysqli_fetch_array($result)) {
                                            $j++;
                                            ?>
                                            <tr>
                                                <td data-title='#'><?php echo $j ?></td>
                                                <?php $userResp = $userClass->getParticularUserData($orderData['order_userId']);
                                                    $userResp = $userResp['UserData'];
                                                    $userName = $userResp['user_name'];
                                                ?>
                                                <td data-title='User Name'><?php echo $userName ?></td>
                                                <td data-title='Order Id'>
                                                    <a href='odet?_=<?php echo $orderData['order_id'] ?>'>
                                                        <?php echo $orderData['order_number'] ?>
                                                    </a>
                                                </td>
                                                <td data-title='Transaction Id'><?php echo $orderData['transaction_id'] ?></td>
                                                <?php
                                                $amount = $orderData['amount'];
                                                setlocale(LC_MONETARY, 'en_IN');
                                                $amount = money_format('%!i', $amount);
                                                ?>
                                                <td data-title='Order Amount'>$<?php echo $amount ?></td>
                                                <td data-title='Order Dated'><?php echo date("d M Y",$orderData['order_dated']) ?></td>
                                                <td data-title='Order Status'><?php echo $orderData['order_status'] ?></td>
                                                <td data-title='Payment Status'><?php echo $orderData['payment_status'] ?></td>
                                                <td data-title='Refund Status'>
                                                    <?php
                                                    $box = "";
                                                    if ($orderData['refund_generated'] == "Yes") {
                                                        $box = "checked";
                                                    }
                                                    $disabled = "disabled";
                                                    if($orderData['payment_status']=="Success" && $orderData['order_status']=='Cancelled'){
                                                        $disabled = "";
                                                    }
                                                    ?>
                                                    <input data-onstyle='info' class='refundstatus' <?php echo $box ?>
                                                    data-toggle='toggle' <?php echo $disabled ?> value="<?php echo $orderData['refund_generated'] ?>"
                                                    id="refund<?php echo $orderData['order_id']?>" onchange=refundProcess("<?php echo $orderData['order_id'] ?>","<?php echo $orderData['order_status'] ?>","<?php echo $orderData['payment_status'] ?>",this) data-size='mini' data-style='ios'
                                                    type='checkbox' />
                                                </td>
                                                <?php if($orderData['payment_status'] != "Success"){
                                                    ?>
                                                    <td data-title='Action'>
                                                        <button class='btn btn-sm btn-danger' title="Order Cancelled Automatically" disabled ) >Cancel Order</button>
                                                    </td>
                                                    <?php
                                                }else{
                                                    if($orderData['order_status'] == "Success") {
                                                        ?>
                                                        <td data-title='Action'>
                                                            <button class='btn btn-sm btn-danger'
                                                                    onclick=cancelOrder('<?php echo $orderData['order_id'] ?>','<?php echo $orderData['order_dated'] ?>')>
                                                                Cancel Order
                                                            </button>
                                                        </td>
                                                        <?php
                                                    }else{
                                                        ?>
                                                        <td data-title='Action'>
                                                            <button class='btn btn-sm btn-warning' disabled>Cancelled</button>
                                                        </td>
                                                        <?php
                                                    }
                                                }
                                                ?>
                                                </tr>
                                            <?php
                                        }
                                    }
                                }
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /page content -->
<?php
include('footer.php');
?>
<script>
    $(document).ready(function () {
        $('#orderTable').DataTable({});
    });
</script>
