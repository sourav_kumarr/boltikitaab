<?php
include('header.php');
$cat_id = $_REQUEST['_'];
echo "<input type='hidden' value=".$cat_id." id='cat_id' />";
?>
<style>
    .boxes{
        background: white;
        min-height:100px;
        border:1px solid #ddd;
        margin-bottom: 10px;
    }
    .Review{
        box-shadow: 3px 3px 2px #ccc;
        margin-bottom: 20px;
    }
</style>
<!-- page content -->
<div class="right_col" role="main">
    <!-- top tiles -->
    <div class="row" role="main">
        <div class="">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2 style="cursor:pointer" onclick="back()"><i class="fa fa-arrow-circle-left"></i> All Categories <small></small></h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li>
                                    <button onclick="window.location='api/excelProcess.php?dataType=particularCat&cat_id=<?php echo $cat_id ?>'" class="btn btn-info btn-sm">Download Excel File</button>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <p class="text-muted font-13 m-b-30">
                                View "<span id="cat_name1"></span>" Detail
                            </p>
                        </div>
                    </div>
                    <div class="col-md-7 boxes">
                        <h2 style="text-align: center">Category Detail</h2>
                        <hr>
                        <div class="col-md-12">
                            <div class="col-md-8" style="padding: 0">
                                <div class="form-group">
                                    <label>Category Name</label>
                                    <p id="cat_name"></p>
                                </div>
                                <div class="form-group">
                                    <label>Created On</label>
                                    <p id="added_on"></p>
                                </div>
                                <div class="form-group">
                                    <label>Books Count</label>
                                    <p id="books_count"></p>
                                </div>
                                <div class="form-group">
                                    <label>Category Visibility</label>
                                    <p id="visibility">Visible</p>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <img id="cat_image" src="images/user.png" class="img-responsive img-thumbnail" />
                            </div>
                        </div>
                        <h4 style="text-align:center;">Books Detail</h4>
                        <hr>
                        <div class="col-md-12" style="height:205px;overflow-y: scroll">
                            <table class="table table-stripped" id="book_detail_table"></table>
                        </div>
                    </div>
                    <div class="col-md-5">
                        <div class="col-md-12 boxes">
                            <h2 style="text-align: center">Total Sales Revenue</h2>
                            <hr>
                            <div class="col-md-12" style="text-align: center;margin-bottom: 20px">
                                <label >Total Sales in March Month</label>
                                <p style="font-size: 20px">$0 /-</p>
                                <label>Select Custom Date Range and Month From Date Picker</label>
                            </div>
                        </div>
                        <div class="col-md-12 boxes" style="max-height:360px;overflow-y: scroll" id="book_detail_div">
                            <h2 style="text-align: center">All Available Books</h2>
                            <hr>
                            <p style="text-align: center"><label>No Books Available</label></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /page content -->
<?php
include('footer.php');
?>
<script>
    var cat_id = $("#cat_id").val();
    var url = "api/categoryProcess.php";
    $.post(url,{"type":"getCategory","cat_id":cat_id} ,function (data) {
        var status = data.Status;
        if (status == "Success") {
            var catData = data.catData;
            $("#cat_name1").html(catData.cat_name);
            $("#cat_name").html(catData.cat_name);
            $("#added_on").html(catData.added_on);
            if (catData.cat_image != "") {
                $("#cat_image").attr("src", "api/Files/images/" + catData.cat_image);
            }
            if (catData.cat_status == "0") {
                $("#visibility").html("InVisible");
            } else {
                $("#visibility").html("Visible");
            }
        }
    });
    $.post(url,{"type":"allBooksData","cat_id":cat_id} ,function (data) {
        var status = data.Status;
        if (status == "Success") {
            var bookCount = data.Count;
            $("#books_count").html(bookCount+" Books Available");
            var bookArray = data.bookArray;
            var book_detail_table = "<tr><th>#</th><th>Book Name</th><th>Author</th><th>Narrator</th><th>Play Time</th></tr>";
            var book_detail_div = "<h2 style='text-align: center'>All Available Books</h2><hr>";
            for(var i=0;i<bookArray.length;i++){
                book_detail_table+="<tr><td>"+(i+1)+"</td><td><a href='bdet.php?_="+bookArray[i].book_id+"'>"+
                bookArray[i].book_name+"</a></td><td>"+bookArray[i].book_author+"</td><td>"+bookArray[i].book_narrator+
                "</td><td>"+bookArray[i].play_time+"</td></tr>";
                var front_look = bookArray[i].front_look;
                if(front_look == ""){
                    front_look = "api/Files/images/img.png";
                }else{
                    front_look = "api/Files/images/"+front_look;
                }
                var box='';
                if(bookArray[i].book_status == "1"){
                    box = 'checked';
                }
                var price = parseFloat(bookArray[i].list_price);
                price = price.toLocaleString();
                book_detail_div+="<div class='col-md-12 Review'><div class='col-md-2'><img src='"+front_look+"' " +
                "class='img-responsive img-thumbnail' /></div><div class='col-md-9'><label>"+bookArray[i].book_name+
                " ( <i onclick=playSong('"+bookArray[i].audio_file+"') class='fa fa-play' style='color:green;cursor:pointer'>" +
                "</i> ) ( $"+price+" )</label><p>"+bookArray[i].book_desc.substr(0,100)+"...</p>" +
                "</div></div>";
            }
            $("#book_detail_table").html(book_detail_table);
            $("#book_detail_div").html(book_detail_div);
        }
    });
</script>
