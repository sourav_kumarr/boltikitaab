<?php
require_once ('Classes/MEMBERSHIP.php');
require_once ('Constants/functions.php');
require_once('Constants/configuration.php');
require_once('Constants/DbConfig.php');
$memberClass = new \Classes\MEMBERSHIP();
$requiredfields = array('type');
($response = RequiredFields($_POST, $requiredfields));
if($response['Status'] == 'Failure'){
    $memberClass->apiResponse($response);
    return false;
}
error_reporting(0);
$type = $_POST['type'];
if($type == "addPlan")
{
    $requiredfields = array('plan_name','renewal_type','plan_price','plan_status');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $memberClass->apiResponse($response);
        return false;
    }
    $plan_name = trim($_POST['plan_name']);
    $plan_price = trim($_POST['plan_price']);
    $renewal_type = trim($_POST['renewal_type']);
    $plan_status = trim($_POST['plan_status']);
    ($response = $memberClass->checkPlanExistence($plan_name));
    if($response[STATUS] == Success) {
        $response = $memberClass->addPlan($plan_name, $plan_price, $renewal_type, $plan_status);
    }
    if($response[STATUS] == Error){
        $memberClass->apiResponse($response);
        return false;
    }
    $planId = $response['planId'];
    $temp = $memberClass->getParticularPlanData($planId);
    $response['planData'] = $temp['planData'];
    $response[ImagesBaseURLKey] = ImagesBaseURL;
    unset($response['catId']);
    $memberClass->apiResponse($response);
}
else if($type == "editPlan")
{
    $requiredfields = array('plan_id','plan_name','plan_status','renewal_type','plan_price');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $memberClass->apiResponse($response);
        return false;
    }
    $plan_id = trim($_POST['plan_id']);
    $plan_name = trim($_POST['plan_name']);
    $plan_price = trim($_POST['plan_price']);
    $renewal_type = trim($_POST['renewal_type']);
    $plan_status = trim($_POST['plan_status']);
    $response = $memberClass->editPlan($plan_id,$plan_name,$plan_price,$renewal_type,$plan_status);
    if($response[STATUS] == Error){
        $memberClass->apiResponse($response);
        return false;
    }
    $planId = $response['planId'];
    $temp = $memberClass->getParticularPlanData($planId);
    $response['planData'] = $temp['planData'];
    unset($response['planId']);
    $memberClass->apiResponse($response);
}
else if($type == "getPlans")
{
    $response = $memberClass->getAllPlans();
    $memberClass->apiResponse($response);
}
else if($type == "getPlan"){
    $requiredfields = array('plan_id');
    if(!RequiredFields($_REQUEST, $requiredfields)) {
        return false;
    }
    $plan_id = $_REQUEST['plan_id'];
    $response = $memberClass->getParticularPlanData($plan_id);
    $memberClass->apiResponse($response);
}
else if($type == "statusChange")
{
    $requiredfields = array('plan_id','value');
    if(!RequiredFields($_REQUEST, $requiredfields)) {
        return false;
    }
    $value = $_REQUEST['value'];
    $plan_id = $_REQUEST['plan_id'];
    $response = $memberClass->statusChange($plan_id,$value);
    if($response[STATUS] == Error) {
        $memberClass->apiResponse($response);
        return false;
    }
    $memberClass->apiResponse($response);
}
else if($type == "deletePlan")
{
    $requiredfields = array('plan_id');
    if(!RequiredFields($_REQUEST, $requiredfields)) {
        return false;
    }
    $plan_id = $_REQUEST['plan_id'];
    $response = $memberClass->deletePlan($plan_id);
    if($response[STATUS] == Error) {
        $memberClass->apiResponse($response);
        return false;
    }
    $memberClass->apiResponse($response);
}
else{
    $response[STATUS] = Error;
    $response[MESSAGE] = "502 UnAuthorised Request";
    $memberClass->apiResponse($response);
}
?>