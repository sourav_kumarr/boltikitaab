<?php
require_once ('Classes/CATEGORY.php');
require_once ('Classes/BOOKS.php');
require_once ('Constants/functions.php');
require_once('Constants/configuration.php');
require_once('Constants/DbConfig.php');
$catClass = new \Classes\CATEGORY();
$booksClass = new \Classes\BOOKS();
$requiredfields = array('type');
($response = RequiredFields($_POST, $requiredfields));
if($response['Status'] == 'Failure'){
    $catClass->apiResponse($response);
    return false;
}
error_reporting(0);
$type = $_POST['type'];
if($type == "addCategory")
{
    $requiredfields = array('cat_name','cat_status');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $catClass->apiResponse($response);
        return false;
    }
    $cat_name = trim($_POST['cat_name']);
    $cat_status = trim($_POST['cat_status']);
    ($response = $catClass->checkCategoryExistence($cat_name));
    if($response[STATUS] == Success) {
        $response = $catClass->addCategory($cat_name,$cat_status);
    }
    if($response[STATUS] == Error){
        $catClass->apiResponse($response);
        return false;
    }
    $catId = $response['catId'];
    $temp = $catClass->getParticularCatData($catId);
    $response['catData'] = $temp['catData'];
    $response[ImagesBaseURLKey] = ImagesBaseURL;
    unset($response['catId']);
    $catClass->apiResponse($response);
}
else if($type == "editCategory")
{
    $requiredfields = array('cat_name','cat_status','cat_id');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $catClass->apiResponse($response);
        return false;
    }
    $cat_name = trim($_POST['cat_name']);
    $cat_status = trim($_POST['cat_status']);
    $cat_id = trim($_POST['cat_id']);
    $imageChanged = trim($_POST['imageChanged']);
    $response = $catClass->editCategory($cat_name,$cat_status,$cat_id,$imageChanged);
    if($response[STATUS] == Error){
        $catClass->apiResponse($response);
        return false;
    }
    $catId = $response['catId'];
    $temp = $catClass->getParticularCatData($catId);
    $response['catData'] = $temp['catData'];
    $response[ImagesBaseURLKey] = ImagesBaseURL;
    unset($response['catId']);
    $catClass->apiResponse($response);
}
else if($type == "mainScreenData"){
    $requiredfields = array('limit');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $catClass->apiResponse($response);
        return false;
    }
    $limit = trim($_POST['limit']);
    ($response = $catClass->getAllCategories());
    if($response[STATUS] == Success){
        $catData = $response['data'];
        for($i=0;$i<sizeof($catData);$i++){
            $cat_id = $catData[$i]['cat_id'];
            $cat_name = $catData[$i]['cat_name'];
            $cat_image = $catData[$i]['cat_image'];
            $added_on = $catData[$i]['added_on'];
            $cat_status = $catData[$i]['cat_status'];
            $books = array();
            $bookresp = $booksClass->getCategoryAllBooks($cat_id,$limit);
            if($bookresp[STATUS] == Success){
                $books = $bookresp['bookData'];
            }
            $mainScreen[]=array("cat_id"=>$cat_id,"cat_name"=>$cat_name,"cat_image"=>$cat_image,"added_on"=>$added_on,
            "cat_status"=>$cat_status,"cat_books"=>$books);
        }
        $response[STATUS] = Success;
        $response[Message] = "data Found";
        unset($response['data']);
        $response['mainScreen'] = $mainScreen;
        $response[AudiosBaseURLKey] = AudiosBaseURL;
        $response[ImagesBaseURLKey] = ImagesBaseURL;
    }
    ($response2 = $booksClass->getTopRatedBooks($limit));
    $topRated = $response2['data'];
    $response['topRated'] = $topRated;
    $catClass->apiResponse($response);
}
else if($type == "getAllTopRated"){
    $requiredfields = array('limit');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $catClass->apiResponse($response);
        return false;
    }
    $limit = trim($_POST['limit']);
    $response = $booksClass->getTopRatedBooks($limit);
    $catClass->apiResponse($response);
}
else if($type == "getCategories")
{
    $response = $catClass->getAllCategories();
    $catClass->apiResponse($response);
}
else if($type == "getCategory"){
    $requiredfields = array('cat_id');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $catClass->apiResponse($response);
        return false;
    }
    $cat_id = $_REQUEST['cat_id'];
    $response = $catClass->getParticularCatData($cat_id);
    $catClass->apiResponse($response);
}
else if($type == "statusChange")
{
    $requiredfields = array('cat_id','value');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $catClass->apiResponse($response);
        return false;
    }
    $value = $_REQUEST['value'];
    $cat_id = $_REQUEST['cat_id'];
    $response = $catClass->statusChange($cat_id,$value);
    if($response[STATUS] == Error) {
        $catClass->apiResponse($response);
        return false;
    }
    $catClass->apiResponse($response);
}
else if($type == "deleteCategory")
{
    $requiredfields = array('cat_id');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $catClass->apiResponse($response);
        return false;
    }
    $cat_id = $_REQUEST['cat_id'];
    $response = $catClass->deleteCategory($cat_id);
    if($response[STATUS] == Error) {
        $catClass->apiResponse($response);
        return false;
    }
    $catClass->apiResponse($response);
}
else if($type='allBooksData'){
    $requiredfields = array('cat_id');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $catClass->apiResponse($response);
        return false;
    }
    $cat_id = $_REQUEST['cat_id'];
    $response = $catClass->getBookCount($cat_id);
    if($response[STATUS] == Error) {
        $catClass->apiResponse($response);
        return false;
    }
    $catClass->apiResponse($response);
}
else{
    $response[STATUS] = Error;
    $response[MESSAGE] = "502 UnAuthorised Request";
    $catClass->apiResponse($response);
}
?>