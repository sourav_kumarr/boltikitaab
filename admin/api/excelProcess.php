<?php
require_once('Constants/configuration.php');
require_once('Constants/DbConfig.php');
require_once('Classes/PHPExcel.php');
require_once('Constants/functions.php');
require_once('Classes/CATEGORY.php');
require_once('Classes/BOOKS.php');
require_once('Classes/DISCOUNT.php');
require_once('Classes/USERCLASS.php');
require_once('Classes/MEMBERSHIP.php');
require_once('Classes/ORDERS.php');
$catClass = new Classes\CATEGORY();
$booksClass = new Classes\BOOKS();
$discountClass = new Classes\DISCOUNT();
$userClass = new Classes\USERCLASS();
$memClass = new Classes\MEMBERSHIP();
$orderClass = new Classes\ORDERS();
$objPHPExcel = new PHPExcel();
// set default font
$objPHPExcel->getDefaultStyle()->getFont()->setName('Calibri');
// set default font size
$objPHPExcel->getDefaultStyle()->getFont()->setSize(10);
// create the writer
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");
/**
 * Define currency and number format.
 */
// currency format, € with < 0 being in red color
$currencyFormat = '#,#0.## \€;[Red]-#,#0.## \€';
// number format, with thousands separator and two decimal points.
$numberFormat = '#,#0.##;[Red]-#,#0.##';
// writer already created the first sheet for us, let's get it
$objSheet = $objPHPExcel->getActiveSheet();
// rename the sheet
$requiredfields = array('dataType');
($response = RequiredFields($_REQUEST, $requiredfields));
if($response['Status'] == 'Failure'){
    apiResponseSend($response);
    return false;
}
$dataType = $_REQUEST['dataType'];
if($dataType == "allCats") {
    $catResponse = $catClass->getAllCategories();
    if ($catResponse[STATUS] == Success) {
        $catData = $catResponse['data'];
        $objSheet->setTitle('Category List');
        // let's bold and size the header font and write the header
        // as you can see, we can specify a range of cells, like here: cells from A1 to A4
        $objSheet->mergeCells('A1:F1');
        $objSheet->getStyle('A1')->getFont()->setSize(12);
        $objSheet->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objSheet->getCell('A1')->setValue('Bolti Kitaab');
        // write Axis header
        $objSheet->mergeCells('A3:F3');
        $objSheet->getStyle('A3')->getFont()->setBold(false)->setSize(11);
        $objSheet->getCell('A3')->setValue('All Categories');

        //Table headings//////
        $objSheet->getStyle('A5:F5')->getFont()->setBold(true)->setSize(10);
        $objSheet->getCell('A5')->setValue('S.No');
        $objSheet->getCell('B5')->setValue('Unique Id');
        $objSheet->getCell('C5')->setValue('Category Name');
        $objSheet->getCell('D5')->setValue('Created Date');
        $objSheet->getCell('E5')->setValue('Books Count');
        $objSheet->getCell('F5')->setValue('Visibility');

        // autosize the columns
        $objSheet->getColumnDimension('A')->setAutoSize(true);
        $objSheet->getColumnDimension('B')->setAutoSize(true);
        $objSheet->getColumnDimension('C')->setAutoSize(true);
        $objSheet->getColumnDimension('D')->setAutoSize(true);
        $objSheet->getColumnDimension('E')->setAutoSize(true);
        $objSheet->getColumnDimension('F')->setAutoSize(true);

        $objSheet->getStyle('A')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objSheet->getStyle('B')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objSheet->getStyle('C')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objSheet->getStyle('D')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objSheet->getStyle('E')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objSheet->getStyle('F')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        // we could get this data from database, but here we are writing for simplicity

        $i = 0;
        $j = $i + 6;
        for (; $i < sizeof($catData); $i++) {
            $sno = $i + 1;
            $countData = $catClass->getBookCount($catData[$i]['cat_id']);
            if($catData[$i]['cat_status'] == "1"){
                $visibility = "Visible";
            }
            else{
                $visibility = "Hidden";
            }
            $objSheet->getCell('A' . $j)->setValue($sno);
            $objSheet->getCell('B' . $j)->setValue($catData[$i]['cat_id']);
            $objSheet->getCell('C' . $j)->setValue($catData[$i]['cat_name']);
            $objSheet->getCell('D' . $j)->setValue($catData[$i]['added_on']);
            $objSheet->getCell('E' . $j)->setValue($countData['Count']." Books Available");
            $objSheet->getCell('F' . $j)->setValue($visibility);
            $j = $j + 1;
        }
        //Setting the header type
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename=BKBCategory'.date("hmis").'.xls');
        header('Cache-Control: max-age=0');
        $objWriter->save('php://output');
        //////End of download Excel
    }
}
else if($dataType == "particularCat") {
    $requiredfields = array('cat_id');
    ($response = RequiredFields($_REQUEST, $requiredfields));
    if($response['Status'] == 'Failure'){
        apiResponseSend($response);
        return false;
    }
    $cat_id = $_REQUEST['cat_id'];
    $catResponse = $catClass->getParticularCatData($cat_id);
    if ($catResponse[STATUS] == Success) {
        $catData = $catResponse['catData'];
        $objSheet->setTitle('Category List');
        // let's bold and size the header font and write the header
        // as you can see, we can specify a range of cells, like here: cells from A1 to A4
        $objSheet->mergeCells('A1:F1');
        $objSheet->getStyle('A1')->getFont()->setSize(12);
        $objSheet->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objSheet->getCell('A1')->setValue('Bolti Kitaab');
        // write Axis header
        $objSheet->mergeCells('A3:F3');
        $objSheet->getStyle('A3')->getFont()->setBold(false)->setSize(11);
        $objSheet->getCell('A3')->setValue('Category:'.$catData['cat_name']);

        //Table headings//////
        $objSheet->getStyle('A6:F6')->getFont()->setBold(true)->setSize(10);
        $objSheet->getCell('A6')->setValue('S.No');
        $objSheet->getCell('B6')->setValue('Unique Id');
        $objSheet->getCell('C6')->setValue('Category Name');
        $objSheet->getCell('D6')->setValue('Created Date');
        $objSheet->getCell('E6')->setValue('Books Count');
        $objSheet->getCell('F6')->setValue('Visibility');

        // autosize the columns
        $objSheet->getColumnDimension('A')->setAutoSize(true);
        $objSheet->getColumnDimension('B')->setAutoSize(true);
        $objSheet->getColumnDimension('C')->setAutoSize(true);
        $objSheet->getColumnDimension('D')->setAutoSize(true);
        $objSheet->getColumnDimension('E')->setAutoSize(true);
        $objSheet->getColumnDimension('F')->setAutoSize(true);
        $objSheet->getColumnDimension('G')->setAutoSize(true);
        $objSheet->getColumnDimension('H')->setAutoSize(true);

        $objSheet->getStyle('A')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objSheet->getStyle('B')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objSheet->getStyle('C')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objSheet->getStyle('D')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objSheet->getStyle('E')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objSheet->getStyle('F')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        // we could get this data from database, but here we are writing for simplicity

        $countData = $catClass->getBookCount($catData['cat_id']);
        if($catData['cat_status'] == "1"){
            $visibility = "Visible";
        }
        else{
            $visibility = "Hidden";
        }
        $objSheet->getCell('A7')->setValue('1');
        $objSheet->getCell('B7')->setValue($catData['cat_id']);
        $objSheet->getCell('C7')->setValue($catData['cat_name']);
        $objSheet->getCell('D7')->setValue($catData['added_on']);
        $objSheet->getCell('E7')->setValue($countData['Count']." Books Available");
        $objSheet->getCell('F7')->setValue($visibility);

        $objSheet->mergeCells('A9:F9');
        $objSheet->getStyle('A9')->getFont()->setSize(11);
        $objSheet->getStyle('A9')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objSheet->getCell('A9')->setValue('Books In Category');

        $objSheet->getStyle('A11:H11')->getFont()->setBold(true)->setSize(10);
        $objSheet->getCell('A11')->setValue('S.No');
        $objSheet->getCell('B11')->setValue('Unique Id');
        $objSheet->getCell('C11')->setValue('Book Name');
        $objSheet->getCell('D11')->setValue('Book Author');
        $objSheet->getCell('E11')->setValue('Book Narrator');
        $objSheet->getCell('F11')->setValue('Book Price');
        $objSheet->getCell('G11')->setValue('Book Description');
        $objSheet->getCell('H11')->setValue('Visibility');

        $i = 0;
        $j = $i + 12;
        if($countData['Count']>0) {
            $bookArray = $countData['bookArray'];
            for (; $i < sizeof($bookArray); $i++) {
                $sno = $i + 1;
                if ($bookArray[$i]['book_status'] == "1") {
                    $visibility = "Visible";
                } else {
                    $visibility = "Hidden";
                }
                $objSheet->getCell('A' . $j)->setValue($sno);
                $objSheet->getCell('B' . $j)->setValue($bookArray[$i]['book_id']);
                $objSheet->getCell('C' . $j)->setValue($bookArray[$i]['book_name']);
                $objSheet->getCell('D' . $j)->setValue($bookArray[$i]['book_author']);
                $objSheet->getCell('E' . $j)->setValue($bookArray[$i]['book_narrator']);
                $objSheet->getCell('F' . $j)->setValue("$" . $bookArray[$i]['list_price']);
                $objSheet->getCell('G' . $j)->setValue($bookArray[$i]['book_desc']);
                $objSheet->getCell('H' . $j)->setValue($visibility);
                $j = $j + 1;
            }
        }
        else{
            $objSheet->mergeCells('A12:H12');
            $objSheet->getCell('A12')->setValue("No Books Available");
        }
        //Setting the header type
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename=BKBCategory'.date("hmis").'.xls');
        header('Cache-Control: max-age=0');
        $objWriter->save('php://output');
        //////End of download Excel
    }
}
else if($dataType == "allBooks") {
    error_reporting(1);
    $bookResponse = $booksClass->getAllBooks();
    if ($bookResponse[STATUS] == Success) {
        $bookData = $bookResponse['data'];
        $objSheet->setTitle('All Books List');
        // let's bold and size the header font and write the header
        // as you can see, we can specify a range of cells, like here: cells from A1 to A4
        $objSheet->mergeCells('A1:J1');
        $objSheet->getStyle('A1')->getFont()->setSize(12);
        $objSheet->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objSheet->getCell('A1')->setValue('Bolti Kitaab');
        // write Axis header
        $objSheet->mergeCells('A3:K3');
        $objSheet->getStyle('A3')->getFont()->setBold(false)->setSize(11);
        $objSheet->getCell('A3')->setValue('All Books');

        //Table headings//////
        $objSheet->getStyle('A5:K5')->getFont()->setBold(true)->setSize(10);
        $objSheet->getCell('A5')->setValue('S.No');
        $objSheet->getCell('B5')->setValue('Unique Id');
        $objSheet->getCell('C5')->setValue('Book Category');
        $objSheet->getCell('D5')->setValue('Book Name');
        $objSheet->getCell('E5')->setValue('Books Author');
        $objSheet->getCell('F5')->setValue('Books Narrator');
        $objSheet->getCell('G5')->setValue('Books Play Time');
        $objSheet->getCell('H5')->setValue('Books List Price');
        $objSheet->getCell('I5')->setValue('Books Discount');
        $objSheet->getCell('J5')->setValue('Visibility');
        $objSheet->getCell('K5')->setValue('Book Description');

        // autosize the columns
        $objSheet->getColumnDimension('A')->setAutoSize(true);
        $objSheet->getColumnDimension('B')->setAutoSize(true);
        $objSheet->getColumnDimension('C')->setAutoSize(true);
        $objSheet->getColumnDimension('D')->setAutoSize(true);
        $objSheet->getColumnDimension('E')->setAutoSize(true);
        $objSheet->getColumnDimension('F')->setAutoSize(true);
        $objSheet->getColumnDimension('G')->setAutoSize(true);
        $objSheet->getColumnDimension('H')->setAutoSize(true);
        $objSheet->getColumnDimension('I')->setAutoSize(true);
        $objSheet->getColumnDimension('J')->setAutoSize(true);
        $objSheet->getColumnDimension('K')->setAutoSize(true);

        $objSheet->getStyle('A')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objSheet->getStyle('B')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objSheet->getStyle('C')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objSheet->getStyle('D')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objSheet->getStyle('E')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objSheet->getStyle('F')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objSheet->getStyle('G')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objSheet->getStyle('H')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objSheet->getStyle('I')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objSheet->getStyle('J')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objSheet->getStyle('K')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        // we could get this data from database, but here we are writing for simplicity

        $i = 0;
        $j = $i + 6;
        for (; $i < sizeof($bookData); $i++) {
            $sno = $i + 1;
            $catData = $catClass->getParticularCatData($bookData[$i]['cat_id']);
            $catData=$catData['catData'];
            if($bookData[$i]['discount_id'] == ""){
                $code = "";
            }else {
                $couponData = $discountClass->getParticularCouponData($bookData[$i]['discount_id']);
                $couponData = $couponData['couponData'];
                $code = $couponData['coupon_code'];
            }
            if($bookData[$i]['book_status'] == "1"){
                $visibility = "Visible";
            }
            else{
                $visibility = "Hidden";
            }
            $objSheet->getCell('A' . $j)->setValue($sno);
            $objSheet->getCell('B' . $j)->setValue($bookData[$i]['book_id']);
            $objSheet->getCell('C' . $j)->setValue($catData['cat_name']);
            $objSheet->getCell('D' . $j)->setValue($bookData[$i]['book_name']);
            $objSheet->getCell('E' . $j)->setValue($bookData[$i]['book_author']);
            $objSheet->getCell('F' . $j)->setValue($bookData[$i]['book_narrator']);
            $objSheet->getCell('G' . $j)->setValue($bookData[$i]['play_time']);
            $objSheet->getCell('H' . $j)->setValue($bookData[$i]['list_price']);
            $objSheet->getCell('I' . $j)->setValue($code);
            $objSheet->getCell('J' . $j)->setValue($visibility);
            $objSheet->getCell('K' . $j)->setValue($bookData[$i]['book_desc']);
            $j = $j + 1;
        }
        //Setting the header type
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename=BKBBooks'.date("hmis").'.xls');
        header('Cache-Control: max-age=0');
        $objWriter->save('php://output');
        //////End of download Excel
    }
    else{
        $response[STATUS] = Error;
        $response[MESSAGE] = "Server Error";
        apiResponseSend($response);
    }
}
else if($dataType == "particularBook") {
    $requiredfields = array('book_id');
    ($response = RequiredFields($_REQUEST, $requiredfields));
    if($response['Status'] == 'Failure'){
        apiResponseSend($response);
        return false;
    }
    $book_id = $_REQUEST['book_id'];
    $bookResponse = $booksClass->getParticularBookData($book_id);
    if ($bookResponse[STATUS] == Success) {
        $bookData = $bookResponse['bookData'];
        $objSheet->setTitle('Book Data');
        // let's bold and size the header font and write the header
        // as you can see, we can specify a range of cells, like here: cells from A1 to A4
        $objSheet->mergeCells('A1:J1');
        $objSheet->getStyle('A1')->getFont()->setSize(12);
        $objSheet->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objSheet->getCell('A1')->setValue('Bolti Kitaab');
        // write Axis header
        $objSheet->mergeCells('A3:J3');
        $objSheet->getStyle('A3')->getFont()->setBold(false)->setSize(11);
        $objSheet->getCell('A3')->setValue('Book:'.$bookData['book_name']);

        //Table headings//////
        $objSheet->getStyle('A5:K5')->getFont()->setBold(true)->setSize(10);
        $objSheet->getCell('A5')->setValue('S.No');
        $objSheet->getCell('B5')->setValue('Unique Id');
        $objSheet->getCell('C5')->setValue('Book Category');
        $objSheet->getCell('D5')->setValue('Book Name');
        $objSheet->getCell('E5')->setValue('Books Author');
        $objSheet->getCell('F5')->setValue('Books Narrator');
        $objSheet->getCell('G5')->setValue('Books Play Time');
        $objSheet->getCell('H5')->setValue('Books List Price');
        $objSheet->getCell('I5')->setValue('Books Discount');
        $objSheet->getCell('J5')->setValue('Visibility');
        $objSheet->getCell('K5')->setValue('Book Description');

        // autosize the columns
        $objSheet->getColumnDimension('A')->setAutoSize(true);
        $objSheet->getColumnDimension('B')->setAutoSize(true);
        $objSheet->getColumnDimension('C')->setAutoSize(true);
        $objSheet->getColumnDimension('D')->setAutoSize(true);
        $objSheet->getColumnDimension('E')->setAutoSize(true);
        $objSheet->getColumnDimension('F')->setAutoSize(true);
        $objSheet->getColumnDimension('G')->setAutoSize(true);
        $objSheet->getColumnDimension('H')->setAutoSize(true);
        $objSheet->getColumnDimension('I')->setAutoSize(true);
        $objSheet->getColumnDimension('J')->setAutoSize(true);
        $objSheet->getColumnDimension('K')->setAutoSize(true);

        $objSheet->getStyle('A')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objSheet->getStyle('B')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objSheet->getStyle('C')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objSheet->getStyle('D')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objSheet->getStyle('E')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objSheet->getStyle('F')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objSheet->getStyle('G')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objSheet->getStyle('H')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objSheet->getStyle('I')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objSheet->getStyle('J')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objSheet->getStyle('K')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        // we could get this data from database, but here we are writing for simplicity

        $catData = $catClass->getParticularCatData($bookData['cat_id']);
        $catData=$catData['catData'];
        if($bookData['discount_id'] == ""){
            $code = "";
        }else {
            $couponData = $discountClass->getParticularCouponData($bookData['discount_id']);
            $couponData = $couponData['couponData'];
            $code = $couponData['coupon_code'];
        }
        if($bookData['book_status'] == "1"){
            $visibility = "Visible";
        }
        else{
            $visibility = "Hidden";
        }
        $objSheet->getCell('A6')->setValue("1");
        $objSheet->getCell('B6')->setValue($bookData['book_id']);
        $objSheet->getCell('C6')->setValue($catData['cat_name']);
        $objSheet->getCell('D6')->setValue($bookData['book_name']);
        $objSheet->getCell('E6')->setValue($bookData['book_author']);
        $objSheet->getCell('F6')->setValue($bookData['book_narrator']);
        $objSheet->getCell('G6')->setValue($bookData['play_time']);
        $objSheet->getCell('H6')->setValue($bookData['list_price']);
        $objSheet->getCell('I6')->setValue($code);
        $objSheet->getCell('J6')->setValue($visibility);
        $objSheet->getCell('K6')->setValue($bookData['book_desc']);


        $rateData = $booksClass->getRatesAndReviews($bookData['book_id']);
        if($rateData[STATUS] != Error) {
            $objSheet->mergeCells('A8:F8');
            $objSheet->getStyle('A8')->getFont()->setSize(11);
            $objSheet->getStyle('A8')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $objSheet->getCell('A8')->setValue("Reviews On Book Average Rate (" . $rateData['userRate'] . ")");

            $objSheet->getStyle('A10:D10')->getFont()->setBold(true)->setSize(10);
            $objSheet->getCell('A10')->setValue('S.No');
            $objSheet->getCell('B10')->setValue('User Name');
            $objSheet->getCell('C10')->setValue('Rating');
            $objSheet->getCell('D10')->setValue('Review');
            $reviews = $rateData['bookData'];
            $i = 0;
            $j = $i + 11;
            for (; $i < sizeof($reviews); $i++) {
                $sno = $i + 1;
                $objSheet->getCell('A' . $j)->setValue($sno);
                $objSheet->getCell('B' . $j)->setValue($reviews[$i]['user_name']);
                $objSheet->getCell('C' . $j)->setValue($reviews[$i]['user_rate']);
                $objSheet->getCell('D' . $j)->setValue($reviews[$i]['user_review']);
                $j = $j + 1;
            }
        }
        else{
            $objSheet->mergeCells('A8:F8');
            $objSheet->getStyle('A8')->getFont()->setSize(11);
            $objSheet->getStyle('A8')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $objSheet->getCell('A8')->setValue("Reviews On Book Average Rate (0.0)");

            $objSheet->getStyle('A10:D10')->getFont()->setBold(true)->setSize(10);
            $objSheet->getCell('A10')->setValue('S.No');
            $objSheet->getCell('B10')->setValue('User Name');
            $objSheet->getCell('C10')->setValue('Rating');
            $objSheet->getCell('D10')->setValue('Review');
            $objSheet->mergeCells('A11:D11');
            $objSheet->getCell('A11')->setValue('No Any Review Found Yet');
        }
        //Setting the header type
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename=BKBCategory' . date("hmis") . '.xls');
        header('Cache-Control: max-age=0');
        $objWriter->save('php://output');
        //////End of download Excel
    }
    else{
        $response[STATUS] = Error;
        $response[MESSAGE] = "Invalid Book Identification";
        apiResponseSend($response);
    }
}
else if($dataType == "allUsers") {
    $userResponse = $userClass->getAllUsers();
    if ($userResponse[STATUS] == Success) {
        $usersData = $userResponse['usersData'];
        $objSheet->setTitle('Users List');
        // let's bold and size the header font and write the header
        // as you can see, we can specify a range of cells, like here: cells from A1 to A4
        $objSheet->mergeCells('A1:F1');
        $objSheet->getStyle('A1')->getFont()->setSize(12);
        $objSheet->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objSheet->getCell('A1')->setValue('Bolti Kitaab');
        // write Axis header
        $objSheet->mergeCells('A3:F3');
        $objSheet->getStyle('A3')->getFont()->setBold(false)->setSize(11);
        $objSheet->getCell('A3')->setValue('All Users');

        //Table headings//////
        $objSheet->getStyle('A5:M5')->getFont()->setBold(true)->setSize(10);
        $objSheet->getCell('A5')->setValue('S.No');
        $objSheet->getCell('B5')->setValue('User Id');
        $objSheet->getCell('C5')->setValue('User Name');
        $objSheet->getCell('D5')->setValue('User Email');
        $objSheet->getCell('E5')->setValue('User Contact');
        $objSheet->getCell('F5')->setValue('Active Plan');
        $objSheet->getCell('G5')->setValue('Activation Date');
        $objSheet->getCell('H5')->setValue('Plan Expiry Date');
        $objSheet->getCell('I5')->setValue('User Address');
        $objSheet->getCell('J5')->setValue('Renewal Type');
        $objSheet->getCell('K5')->setValue('Auto Renewal');
        $objSheet->getCell('L5')->setValue('Email Verified');
        $objSheet->getCell('M5')->setValue('Visibility');

        // autosize the columns
        $objSheet->getColumnDimension('A')->setAutoSize(true);
        $objSheet->getColumnDimension('B')->setAutoSize(true);
        $objSheet->getColumnDimension('C')->setAutoSize(true);
        $objSheet->getColumnDimension('D')->setAutoSize(true);
        $objSheet->getColumnDimension('E')->setAutoSize(true);
        $objSheet->getColumnDimension('F')->setAutoSize(true);
        $objSheet->getColumnDimension('G')->setAutoSize(true);
        $objSheet->getColumnDimension('H')->setAutoSize(true);
        $objSheet->getColumnDimension('I')->setAutoSize(true);
        $objSheet->getColumnDimension('J')->setAutoSize(true);
        $objSheet->getColumnDimension('K')->setAutoSize(true);
        $objSheet->getColumnDimension('L')->setAutoSize(true);
        $objSheet->getColumnDimension('M')->setAutoSize(true);
        $objSheet->getColumnDimension('N')->setAutoSize(true);

        $objSheet->getStyle('A')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objSheet->getStyle('B')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objSheet->getStyle('C')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objSheet->getStyle('D')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objSheet->getStyle('E')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objSheet->getStyle('F')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objSheet->getStyle('G')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objSheet->getStyle('H')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objSheet->getStyle('I')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objSheet->getStyle('J')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objSheet->getStyle('K')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objSheet->getStyle('L')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objSheet->getStyle('M')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objSheet->getStyle('N')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        // we could get this data from database, but here we are writing for simplicity

        $i = 0;
        $j = $i + 6;
        for (; $i < sizeof($usersData); $i++) {
            $sno = $i + 1;
            $planData = $memClass->getParticularPlanData($usersData[$i]['active_plan']);
            $planData = $planData['planData'];
            $plan_name = $planData['plan_name'];
            if($usersData[$i]['user_status'] == "1"){
                $visibility = "Visible";
            }
            else{
                $visibility = "Hidden";
            }
            if($usersData[$i]['auto_renewal'] == "1"){
                $autoRenewal = "On";
            }
            else{
                $autoRenewal = "Off";
            }
            $plan_activation = date("d M Y",$usersData[$i]['activation_date']);
            $plan_expiry = date("d M Y",$usersData[$i]['plan_expiry_date']);
            $objSheet->getCell('A' . $j)->setValue($sno);
            $objSheet->getCell('B' . $j)->setValue($usersData[$i]['user_id']);
            $objSheet->getCell('C' . $j)->setValue($usersData[$i]['user_name']);
            $objSheet->getCell('D' . $j)->setValue($usersData[$i]['user_email']);
            $objSheet->getCell('E' . $j)->setValue($usersData[$i]['user_contact']);
            $objSheet->getCell('F' . $j)->setValue($plan_name);
            $objSheet->getCell('G' . $j)->setValue($plan_activation);
            $objSheet->getCell('H' . $j)->setValue($plan_expiry);
            $objSheet->getCell('I' . $j)->setValue($usersData[$i]['user_address']);
            $objSheet->getCell('J' . $j)->setValue($usersData[$i]['renewal_type']);
            $objSheet->getCell('K' . $j)->setValue($autoRenewal);
            $objSheet->getCell('L' . $j)->setValue($usersData[$i]['email_verified']);
            $objSheet->getCell('M' . $j)->setValue($visibility);
            $j = $j + 1;
        }
        //Setting the header type
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename=BKBCategory'.date("hmis").'.xls');
        header('Cache-Control: max-age=0');
        $objWriter->save('php://output');
        //////End of download Excel
    }
    else{
        $response[STATUS] = Error;
        $response[MESSAGE] = "No Users Found";
        apiResponseSend($response);
    }
}
else if($dataType == "allPlans") {
    $planResponse = $memClass->getAllPlans();
    if ($planResponse[STATUS] == Success) {
        $planData = $planResponse['data'];
        $objSheet->setTitle('MemberShip Plans List');
        // let's bold and size the header font and write the header
        // as you can see, we can specify a range of cells, like here: cells from A1 to A4
        $objSheet->mergeCells('A1:F1');
        $objSheet->getStyle('A1')->getFont()->setSize(12);
        $objSheet->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objSheet->getCell('A1')->setValue('Bolti Kitaab');
        // write Axis header
        $objSheet->mergeCells('A3:F3');
        $objSheet->getStyle('A3')->getFont()->setBold(false)->setSize(11);
        $objSheet->getCell('A3')->setValue('All Membership Plans');

        //Table headings//////
        $objSheet->getStyle('A5:F5')->getFont()->setBold(true)->setSize(10);
        $objSheet->getCell('A5')->setValue('S.No');
        $objSheet->getCell('B5')->setValue('Unique Id');
        $objSheet->getCell('C5')->setValue('Plan Name');
        $objSheet->getCell('D5')->setValue('Plan Price');
        $objSheet->getCell('E5')->setValue('Renewal Methods');
        $objSheet->getCell('F5')->setValue('Visibility');

        // autosize the columns
        $objSheet->getColumnDimension('A')->setAutoSize(true);
        $objSheet->getColumnDimension('B')->setAutoSize(true);
        $objSheet->getColumnDimension('C')->setAutoSize(true);
        $objSheet->getColumnDimension('D')->setAutoSize(true);
        $objSheet->getColumnDimension('E')->setAutoSize(true);
        $objSheet->getColumnDimension('F')->setAutoSize(true);

        $objSheet->getStyle('A')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objSheet->getStyle('B')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objSheet->getStyle('C')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objSheet->getStyle('D')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objSheet->getStyle('E')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objSheet->getStyle('F')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        // we could get this data from database, but here we are writing for simplicity

        $i = 0;
        $j = $i + 6;
        for (; $i < sizeof($planData); $i++) {
            $sno = $i + 1;
            $plan_name = $planData[$i]['plan_name'];
            if($planData[$i]['plan_status'] == "1"){
                $visibility = "Visible";
            }
            else{
                $visibility = "Hidden";
            }
            $plan_renewalMethod = $planData[$i]['renewal_type'];
            if($plan_renewalMethod == "all"){
                $plan_renewalMethod = "Monthly, Yearly";
            }
            $objSheet->getCell('A' . $j)->setValue($sno);
            $objSheet->getCell('B' . $j)->setValue($planData[$i]['plan_id']);
            $objSheet->getCell('C' . $j)->setValue($plan_name);
            $objSheet->getCell('D' . $j)->setValue("$".$planData[$i]['plan_price']);
            $objSheet->getCell('E' . $j)->setValue($plan_renewalMethod);
            $objSheet->getCell('F' . $j)->setValue($visibility);
            $j = $j + 1;
        }
        //Setting the header type
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename=BKBCategory'.date("hmis").'.xls');
        header('Cache-Control: max-age=0');
        $objWriter->save('php://output');
        //////End of download Excel
    }
    else{
        $response[STATUS] = Error;
        $response[MESSAGE] = "No Users Found";
        apiResponseSend($response);
    }
}
else if($dataType == "allCoupons") {
    $couponResponse = $discountClass->getAllCoupons();
    if ($couponResponse[STATUS] == Success) {
        $couponData = $couponResponse['data'];
        $objSheet->setTitle('Discount Coupons List');
        // let's bold and size the header font and write the header
        // as you can see, we can specify a range of cells, like here: cells from A1 to A4
        $objSheet->mergeCells('A1:F1');
        $objSheet->getStyle('A1')->getFont()->setSize(12);
        $objSheet->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objSheet->getCell('A1')->setValue('Bolti Kitaab');
        // write Axis header
        $objSheet->mergeCells('A3:F3');
        $objSheet->getStyle('A3')->getFont()->setBold(false)->setSize(11);
        $objSheet->getCell('A3')->setValue('All Discount Coupons');

        //Table headings//////
        $objSheet->getStyle('A5:G5')->getFont()->setBold(true)->setSize(10);
        $objSheet->getCell('A5')->setValue('S.No');
        $objSheet->getCell('B5')->setValue('Unique Id');
        $objSheet->getCell('C5')->setValue('Coupon Code');
        $objSheet->getCell('D5')->setValue('Coupon Value');
        $objSheet->getCell('E5')->setValue('Generated On');
        $objSheet->getCell('F5')->setValue('Expired On');
        $objSheet->getCell('G5')->setValue('Visibility');

        // autosize the columns
        $objSheet->getColumnDimension('A')->setAutoSize(true);
        $objSheet->getColumnDimension('B')->setAutoSize(true);
        $objSheet->getColumnDimension('C')->setAutoSize(true);
        $objSheet->getColumnDimension('D')->setAutoSize(true);
        $objSheet->getColumnDimension('E')->setAutoSize(true);
        $objSheet->getColumnDimension('F')->setAutoSize(true);
        $objSheet->getColumnDimension('G')->setAutoSize(true);

        $objSheet->getStyle('A')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objSheet->getStyle('B')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objSheet->getStyle('C')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objSheet->getStyle('D')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objSheet->getStyle('E')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objSheet->getStyle('F')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objSheet->getStyle('G')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        // we could get this data from database, but here we are writing for simplicity

        $i = 0;
        $j = $i + 6;
        for (; $i < sizeof($couponData); $i++) {
            $sno = $i + 1;
            $generatedDate = date("d M Y",$couponData[$i]['generated_on']);
            if($couponData[$i]['coupon_status'] == "1"){
                $visibility = "Visible";
            }
            else{
                $visibility = "Hidden";
            }
            $objSheet->getCell('A' . $j)->setValue($sno);
            $objSheet->getCell('B' . $j)->setValue($couponData[$i]['coupon_id']);
            $objSheet->getCell('C' . $j)->setValue($couponData[$i]['coupon_code']);
            $objSheet->getCell('D' . $j)->setValue($couponData[$i]['coupon_value']);
            $objSheet->getCell('E' . $j)->setValue($generatedDate);
            $objSheet->getCell('F' . $j)->setValue($couponData[$i]['expired_on']);
            $objSheet->getCell('G' . $j)->setValue($visibility);
            $j = $j + 1;
        }
        //Setting the header type
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename=BKBCategory'.date("hmis").'.xls');
        header('Cache-Control: max-age=0');
        $objWriter->save('php://output');
        //////End of download Excel
    }
    else{
        $response[STATUS] = Error;
        $response[MESSAGE] = "No Users Found";
        apiResponseSend($response);
    }
}
else if($dataType == "allOrders") {
    $orderResponse = $orderClass->getAllOrders();
    if ($orderResponse[STATUS] == Success) {
        $orderData = $orderResponse['order'];
        $objSheet->setTitle('All Orders List');
        // let's bold and size the header font and write the header
        // as you can see, we can specify a range of cells, like here: cells from A1 to A4
        $objSheet->mergeCells('A1:H1');
        $objSheet->getStyle('A1')->getFont()->setSize(12);
        $objSheet->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objSheet->getCell('A1')->setValue('Bolti Kitaab');
        // write Axis header
        $objSheet->mergeCells('A3:H3');
        $objSheet->getStyle('A3')->getFont()->setBold(false)->setSize(11);
        $objSheet->getCell('A3')->setValue('All Generated Orders');

        //Table headings//////
        $objSheet->getStyle('A5:H5')->getFont()->setBold(true)->setSize(10);
        $objSheet->getCell('A5')->setValue('S.No');
        $objSheet->getCell('B5')->setValue('Unique Id');
        $objSheet->getCell('C5')->setValue('Order Number');
        $objSheet->getCell('D5')->setValue('Transaction Id');
        $objSheet->getCell('E5')->setValue('Order Amount');
        $objSheet->getCell('F5')->setValue('Payment Status');
        $objSheet->getCell('G5')->setValue('Order Dated');
        $objSheet->getCell('H5')->setValue('User Name');

        // autosize the columns
        $objSheet->getColumnDimension('A')->setAutoSize(true);
        $objSheet->getColumnDimension('B')->setAutoSize(true);
        $objSheet->getColumnDimension('C')->setAutoSize(true);
        $objSheet->getColumnDimension('D')->setAutoSize(true);
        $objSheet->getColumnDimension('E')->setAutoSize(true);
        $objSheet->getColumnDimension('F')->setAutoSize(true);
        $objSheet->getColumnDimension('G')->setAutoSize(true);
        $objSheet->getColumnDimension('H')->setAutoSize(true);

        $objSheet->getStyle('A')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objSheet->getStyle('B')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objSheet->getStyle('C')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objSheet->getStyle('D')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objSheet->getStyle('E')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objSheet->getStyle('F')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objSheet->getStyle('G')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objSheet->getStyle('H')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        // we could get this data from database, but here we are writing for simplicity

        $i = 0;
        $j = $i + 6;
        for (; $i < sizeof($orderData); $i++) {
            $sno = $i + 1;
            $usersData = $userClass->getParticularUserData($orderData[$i]['order_user_id']);
            $usersData = $usersData['UserData'];
            $objSheet->getCell('A' . $j)->setValue($sno);
            $objSheet->getCell('B' . $j)->setValue($orderData[$i]['order_id']);
            $objSheet->getCell('C' . $j)->setValue($orderData[$i]['order_number']);
            $objSheet->getCell('D' . $j)->setValue($orderData[$i]['transaction_id']);
            $objSheet->getCell('E' . $j)->setValue("$".$orderData[$i]['amount']);
            $objSheet->getCell('F' . $j)->setValue($orderData[$i]['payment_status']);
            $objSheet->getCell('G' . $j)->setValue($orderData[$i]['order_dated']);
            $objSheet->getCell('H' . $j)->setValue($usersData['user_name']);
            $j = $j + 1;
        }
        //Setting the header type
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename=BKBCategory'.date("hmis").'.xls');
        header('Cache-Control: max-age=0');
        $objWriter->save('php://output');
        //////End of download Excel
    }
    else{
        $response[STATUS] = Error;
        $response[MESSAGE] = "No Users Found";
        apiResponseSend($response);
    }
}
else if($dataType == "particularOrder") {
    $requiredfields = array('order_id');
    ($response = RequiredFields($_REQUEST, $requiredfields));
    if($response['Status'] == 'Failure'){
        apiResponseSend($response);
        return false;
    }
    $order_id = $_REQUEST['order_id'];
    $orderResponse = $orderClass->getParticularOrderData($order_id);
    if ($orderResponse[STATUS] == Success) {
        $orderData = $orderResponse['order'];
        $objSheet->setTitle('Particular Order Detail');
        // let's bold and size the header font and write the header
        // as you can see, we can specify a range of cells, like here: cells from A1 to A4
        $objSheet->mergeCells('A1:H1');
        $objSheet->getStyle('A1')->getFont()->setSize(12);
        $objSheet->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objSheet->getCell('A1')->setValue('Bolti Kitaab');
        // write Axis header
        $objSheet->mergeCells('A3:J3');
        $objSheet->getStyle('A3')->getFont()->setBold(false)->setSize(11);
        $objSheet->getCell('A3')->setValue('Order:'.$orderData['order_number']);

        //Table headings//////
        $objSheet->getStyle('A5:K5')->getFont()->setBold(true)->setSize(10);
        $objSheet->getCell('A5')->setValue('S.No');
        $objSheet->getCell('B5')->setValue('Unique Id');
        $objSheet->getCell('C5')->setValue('Order Number');
        $objSheet->getCell('D5')->setValue('Transaction Id');
        $objSheet->getCell('E5')->setValue('Order Amount');
        $objSheet->getCell('F5')->setValue('Payment Status');
        $objSheet->getCell('G5')->setValue('Order Dated');
        $objSheet->getCell('H5')->setValue('User Name');

        // autosize the columns
        $objSheet->getColumnDimension('A')->setAutoSize(true);
        $objSheet->getColumnDimension('B')->setAutoSize(true);
        $objSheet->getColumnDimension('C')->setAutoSize(true);
        $objSheet->getColumnDimension('D')->setAutoSize(true);
        $objSheet->getColumnDimension('E')->setAutoSize(true);
        $objSheet->getColumnDimension('F')->setAutoSize(true);
        $objSheet->getColumnDimension('G')->setAutoSize(true);
        $objSheet->getColumnDimension('H')->setAutoSize(true);


        $objSheet->getStyle('A')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objSheet->getStyle('B')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objSheet->getStyle('C')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objSheet->getStyle('D')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objSheet->getStyle('E')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objSheet->getStyle('F')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objSheet->getStyle('G')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objSheet->getStyle('H')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        // we could get this data from database, but here we are writing for simplicity

        $usersData = $userClass->getParticularUserData($orderData['order_userId']);
        $usersData = $usersData['UserData'];
        $objSheet->getCell('A6')->setValue("1");
        $objSheet->getCell('B6')->setValue($orderData['order_id']);
        $objSheet->getCell('C6')->setValue($orderData['order_number']);
        $objSheet->getCell('D6')->setValue($orderData['transaction_id']);
        $objSheet->getCell('E6')->setValue("$".$orderData['amount']);
        $objSheet->getCell('F6')->setValue($orderData['payment_status']);
        $objSheet->getCell('G6')->setValue($orderData['order_dated']);
        $objSheet->getCell('H6')->setValue($usersData['user_name']);

        $objSheet->mergeCells('A8:H8');
        $objSheet->getStyle('A8')->getFont()->setSize(11);
        $objSheet->getStyle('A8')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objSheet->getCell('A8')->setValue("Order Contains Items");

        $objSheet->getStyle('A10:I10')->getFont()->setBold(true)->setSize(10);
        $objSheet->getCell('A10')->setValue('S.No');
        $objSheet->getCell('B10')->setValue('Unique Id');
        $objSheet->getCell('C10')->setValue('Book Category');
        $objSheet->getCell('D10')->setValue('Book Name');
        $objSheet->getCell('E10')->setValue('Books Author');
        $objSheet->getCell('F10')->setValue('Books Narrator');
        $objSheet->getCell('G10')->setValue('Books Play Time');
        $objSheet->getCell('H10')->setValue('Books List Price');

        $detail = $orderDetail = $orderData['order_detail'];
        $i = 0;
        $j = $i + 11;
        for (; $i < sizeof($detail); $i++) {
            $catData = $catClass->getParticularCatData($detail[$i]['cat_id']);
            $catData=$catData['catData'];
            $sno = $i + 1;
            $objSheet->getCell('A' . $j)->setValue($sno);
            $objSheet->getCell('B' . $j)->setValue($detail[$i]['detail_id']);
            $objSheet->getCell('C' . $j)->setValue($catData['cat_name']);
            $objSheet->getCell('D' . $j)->setValue($detail[$i]['book_name']);
            $objSheet->getCell('E' . $j)->setValue($detail[$i]['book_author']);
            $objSheet->getCell('F' . $j)->setValue($detail[$i]['book_narrator']);
            $objSheet->getCell('G' . $j)->setValue($detail[$i]['play_time']);
            $objSheet->getCell('H' . $j)->setValue("$".$detail[$i]['item_price']);
            $j = $j + 1;
        }
        //Setting the header type
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename=BKBCategory' . date("hmis") . '.xls');
        header('Cache-Control: max-age=0');
        $objWriter->save('php://output');
        //////End of download Excel
    }
    else{
        $response[STATUS] = Error;
        $response[MESSAGE] = "Invalid Order Identification";
        apiResponseSend($response);
    }
}
else{
    $response[STATUS] = Error;
    $response[MESSAGE] = "502 UnAuthorised Request";
    apiResponseSend($response);
}
function apiResponseSend($response){
    header("Content-Type: application/json");
    echo json_encode($response);
}
?>