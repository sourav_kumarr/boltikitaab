<?php
/**
 * Created by PhpStorm.
 * User: sunil
 * Date: 3/20/2017
 * Time: 11:23 AM
 */

namespace Classes;
require_once('CONNECT.php');
class CATEGORY
{
    public $link = null;
    public $response = array();

    function __construct()
    {
        $this->link = new CONNECT();
        $this->currentDateTime = date('d M Y h:i:s A');
        $this->currentDateTimeStamp = strtotime($this->currentDateTime);
    }
    public function addCategory($cat_name,$cat_status)
    {
        $link = $this->link->connect();
        if ($link) {
            $file_response = $this->link->storeImage('cat_image', 'images');
            if ($file_response[STATUS] == Error) {
                $this->response[STATUS] = $file_response[STATUS];
                $this->response[MESSAGE] = $file_response[MESSAGE];
                return $this->response;
            }
            $file_name = $file_response['File_Name'];
            $query = "insert into categories (cat_name,cat_image,added_on,cat_status) VALUES ('$cat_name','$file_name',
            '$this->currentDateTime','$cat_status')";
            $result = mysqli_query($link, $query);
            if ($result) {
                $this->response[STATUS] = Success;
                $this->response[MESSAGE] = "New Category Added SuccessFully";
                $this->response['catId'] = $this->link->getLastId();
            } else {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        } else {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
        }
        return $this->response;
    }
    public function editCategory($cat_name,$cat_status,$cat_id,$imageChanged)
    {
        $link = $this->link->connect();
        if ($link) {
            if($imageChanged == "yes"){
                $file_response = $this->link->storeImage('cat_image','images');
                if ($file_response[STATUS] == Error) {
                    $this->response[STATUS] = $file_response[STATUS];
                    $this->response[MESSAGE] = $file_response[MESSAGE];
                    return $this->response;
                }
                $file_name = $file_response['File_Name'];
                $query = "update categories set cat_name='$cat_name', cat_status='$cat_status', cat_image='$file_name'
                where cat_id = '$cat_id'";
            }
            else{
                $query = "update categories set cat_name='$cat_name', cat_status='$cat_status' where cat_id = '$cat_id'";
            }
            $result = mysqli_query($link, $query);
            if ($result) {
                $this->response[STATUS] = Success;
                $this->response[MESSAGE] = "Category Updated SuccessFully";
                $this->response['catId'] = $cat_id;
            } else {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        } else {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
        }
        return $this->response;
    }
    public function checkCategoryExistence($cat_name)
    {
        $link = $this->link->connect();
        if ($link) {
            $query = "select * from categories where cat_name = '$cat_name'";
            $result = mysqli_query($link, $query);
            if ($result) {
                $num = mysqli_num_rows($result);
                if ($num > 0) {
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = "Category Name Already Existance Please Use Diffrent One";
                    $row = mysqli_fetch_array($result);
                    $this->response['catId'] = $row['cat_id'];
                } else {
                    $this->response[STATUS] = Success;
                    $this->response[MESSAGE] = "Valid Name";
                }
            } else {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        } else {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
        }
        return $this->response;
    }
    public function getParticularCatData($catId)
    {
        $link = $this->link->connect();
        if($link) {
            $query="select * from categories where cat_id='$catId'";
            $result = mysqli_query($link,$query);
            if($result)
            {
                $num = mysqli_num_rows($result);
                if($num>0) {
                    $catData = mysqli_fetch_assoc($result);
                    $this->response[STATUS] = Success;
                    $this->response[MESSAGE] = "Category Exist";
                    $this->response['catData'] = $catData;
                }
                else{
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = "Invalid Category";
                }
            }
            else
            {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else
        {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
        }
        return $this->response;
    }
    public function getAllCategories()
    {
        $catArray = array();
        $link = $this->link->connect();
        if($link) {
            $query="select * from categories order by cat_id DESC";
            $result = mysqli_query($link,$query);
            if($result)
            {
                $num = mysqli_num_rows($result);
                if($num>0) {
                    while($catData = mysqli_fetch_array($result)) {
                        $catArray[]=array(
                            "cat_id"=>$catData['cat_id'],
                            "cat_name"=>$catData['cat_name'],
                            "cat_image"=>$catData['cat_image'],
                            "added_on"=>$catData['added_on'],
                            "cat_status"=>$catData['cat_status']
                        );
                    }
                    $this->response[STATUS] = Success;
                    $this->response[MESSAGE] = "Data Found";
                    $this->response['data'] = $catArray;
                }
                else{
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = "No Categories Found";
                }
            }
            else
            {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else
        {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
        }
        return $this->response;
    }
    public function getBookCount($cat_id)
    {
        $link = $this->link->connect();
        if($link) {
            $query="select * from books where cat_id = '$cat_id'";
            $result = mysqli_query($link,$query);
            if($result)
            {
                $num = mysqli_num_rows($result);
                if($num>0){
                    while($rows = mysqli_fetch_array($result)){
                        $books[]=array(
                            "book_id"=>$rows['book_id'],
                            "book_name"=>$rows['book_name'],
                            "book_author"=>$rows['book_author'],
                            "book_narrator"=>$rows['book_narrator'],
                            "play_time"=>$rows['play_time'],
                            "book_desc"=>$rows['book_desc'],
                            "list_price"=>$rows['list_price'],
                            "book_status"=>$rows['book_status'],
                            "audio_file"=>$rows['audio_file'],
                            "front_look"=>$rows['front_look']
                        );
                    }
                    $this->response[STATUS] = Success;
                    $this->response[MESSAGE] = "Count Exist";
                    $this->response["Count"] = $num;
                    $this->response["bookArray"] = $books;
                }
                else{
                    $this->response[STATUS] = Success;
                    $this->response[MESSAGE] = "Count Exist";
                    $this->response["Count"] = $num;
                }

            }
            else
            {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
                $this->response["Count"] = 0;
            }
        }
        else
        {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
            $this->response["Count"] = 0;
        }
        return $this->response;
    }
    public function statusChange($cat_id,$value){
        $link = $this->link->connect();
        if($link) {
            $query = "select * from categories where cat_id='$cat_id'";
            $result = mysqli_query($link, $query);
            if ($result) {
                $num = mysqli_num_rows($result);
                if ($num > 0) {
                    $update = mysqli_query($link, "UPDATE categories SET cat_status='$value' WHERE cat_id='$cat_id'");
                    if ($update) {
                        $this->response[STATUS] = Success;
                        $this->response[MESSAGE] = "Status Has Been Changed Successfully";
                    } else {
                        $this->response[STATUS] = Error;
                        $this->response[MESSAGE] = $this->link->sqlError();
                    }
                } else {
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = "UnAuthorized Access";
                }
            } else {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else{
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = "Connection Error";
        }
        return $this->response;
    }
    public function deleteCategory($cat_id){
        $link = $this->link->connect();
        if($link) {
            $query = "select * from categories where cat_id='$cat_id'";
            $result = mysqli_query($link, $query);
            if ($result) {
                $num = mysqli_num_rows($result);
                if ($num > 0) {
                    $update = mysqli_query($link, "delete from categories WHERE cat_id='$cat_id'");
                    if ($update) {
                        $row = mysqli_fetch_array($result);
                        unlink("Files/images/".$row['cat_image']);
                        $this->response[STATUS] = Success;
                        $this->response[MESSAGE] = "Category Has Been Deleted Successfully";
                    } else {
                        $this->response[STATUS] = Error;
                        $this->response[MESSAGE] = $this->link->sqlError();
                    }
                } else {
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = "UnAuthorized Access";
                }
            } else {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else{
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = "Connection Error";
        }
        return $this->response;
    }
    public function apiResponse($response)
    {
        header("Content-Type: application/json");
        echo json_encode($response);
    }
}