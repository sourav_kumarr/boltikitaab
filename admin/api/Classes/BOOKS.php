<?php
/**
 * Created by PhpStorm.
 * User: sunil
 * Date: 3/21/2017
 * Time: 5:36 PM
 */

namespace Classes;
require_once('CONNECT.php');
require_once('USERCLASS.php');
require_once('CATEGORY.php');
class BOOKS
{
    public $userClass = null;
    public $category = null;
    public $link = null;
    public $response = array();

    function __construct()
    {
        $this->link = new CONNECT();
        $this->userClass = new USERCLASS();
        $this->category = new CATEGORY();
        $this->currentDateTime = date('d M Y h:i:s A');
        $this->currentDateTimeStamp = strtotime($this->currentDateTime);
    }
    public function addBook($cat_id,$book_title,$book_desc,$book_author,$book_narrator,$org_price,$dis_price,$book_status,$discount_on_book)
    {
        $link = $this->link->connect();
        if ($link) {
            $audio_response = $this->link->storeImage('book_audio','audio');
            if ($audio_response[STATUS] == Error) {

                $this->response[STATUS] = $audio_response[STATUS];
                $this->response[MESSAGE] = $audio_response[MESSAGE];
                return $this->response;
            }
            $audio_file = $audio_response['File_Name'];
            $short_audio_response = $this->link->storeImage('book_short_audio','audio');
            if ($short_audio_response[STATUS] == Error) {

                $this->response[STATUS] = $short_audio_response[STATUS];
                $this->response[MESSAGE] = $short_audio_response[MESSAGE];
                return $this->response;
            }
            $short_audio_file = $short_audio_response['File_Name'];
            $image_response = $this->link->storeImage('book_image','images');
            if ($image_response[STATUS] == Error) {
                $this->response[STATUS] = $image_response[STATUS];
                $this->response[MESSAGE] = $image_response[MESSAGE];
                return $this->response;
            }
            $image_file = $image_response['File_Name'];
            $mp3file = new \MP3File("Files/audio/".$audio_file);
            $duration2 = $mp3file->getDuration();//(slower) for VBR (or CBR)
            $finalDuration = \MP3File::formatTime($duration2);
            $query = "insert into books (cat_id,book_name,book_desc,book_author,book_narrator,play_time,
            list_price,discounted_price,discount_id,book_status,audio_file,short_audio_file,front_look,average_rate) VALUES 
            ('$cat_id','$book_title','$book_desc','$book_author','$book_narrator','$finalDuration','$org_price',
            '$dis_price','$discount_on_book','$book_status','$audio_file','$short_audio_file','$image_file','5.0')";
            $result = mysqli_query($link, $query);
            if ($result) {

                $this->response[STATUS] = Success;
                $this->response[MESSAGE] = "New Book Added SuccessFully";
                $this->response['catId'] = $this->link->getLastId();
            } else {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        } else {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
        }
        return $this->response;
    }
    public function editBook($book_id,$book_title,$book_desc,$book_author,$book_narrator,$org_price,$dis_price,$cat_id,
    $book_status,$imageChanged,$audioChanged,$shortaudioChanged,$discount_on_book)
    {
        $link = $this->link->connect();
        if ($link) {
            $book_audio="";
            $image_name="";
            if($imageChanged == "yes") {
                $image_response = $this->link->storeImage('book_image', 'images');
                if ($image_response[STATUS] == Error) {
                    $this->response[STATUS] = $image_response[STATUS];
                    $this->response[MESSAGE] = $image_response[MESSAGE];
                    return $this->response;
                }
                $image_name = $image_response['File_Name'];
            }
            if($audioChanged == "yes") {
                $audio_response = $this->link->storeImage('book_audio', 'audio');
                if ($audio_response[STATUS] == Error) {
                    $this->response[STATUS] = $audio_response[STATUS];
                    $this->response[MESSAGE] = $audio_response[MESSAGE];
                    return $this->response;
                }
                $book_audio = $audio_response['File_Name'];
                $mp3file = new \MP3File("Files/audio/".$book_audio);
                $duration2 = $mp3file->getDuration();//(slower) for VBR (or CBR)
                $finalDuration = \MP3File::formatTime($duration2);
            }
            if($shortaudioChanged == "yes") {
                $short_audio_response = $this->link->storeImage('book_short_audio', 'audio');
                if ($short_audio_response[STATUS] == Error) {
                    $this->response[STATUS] = $short_audio_response[STATUS];
                    $this->response[MESSAGE] = $short_audio_response[MESSAGE];
                    return $this->response;
                }
                $book_short_audio = $short_audio_response['File_Name'];
            }
            if($imageChanged == "yes" && $audioChanged == "yes" && $shortaudioChanged == "yes"){
                $query = "update books set cat_id='$cat_id',book_name='$book_title',book_desc='$book_desc',
                book_author='$book_author', book_narrator='$book_narrator',play_time='$finalDuration',
                list_price='$org_price',discounted_price='$dis_price',discount_id='$discount_on_book', book_status='$book_status',
                audio_file='$book_audio',short_audio_file='$book_short_audio',front_look='$image_name' 
                where book_id = '$book_id'";
            }
            else if($imageChanged == "yes" && $audioChanged == "no" && $shortaudioChanged == "no"){
                $query = "update books set cat_id='$cat_id',book_name='$book_title',book_desc='$book_desc',
                book_author='$book_author', book_narrator='$book_narrator',play_time='$finalDuration',
                list_price='$org_price',discounted_price='$dis_price',discount_id='$discount_on_book', book_status='$book_status',
                front_look='$image_name' where book_id = '$book_id'";
            }
            else if($imageChanged == "yes" && $audioChanged == "yes" && $shortaudioChanged == "no"){
                $query = "update books set cat_id='$cat_id',book_name='$book_title',book_desc='$book_desc',
                book_author='$book_author', book_narrator='$book_narrator',play_time='$finalDuration',
                list_price='$org_price',discounted_price='$dis_price',discount_id='$discount_on_book', book_status='$book_status',
                audio_file='$book_audio',front_look='$image_name' where book_id = '$book_id'";
            }
            else if($imageChanged == "yes" && $audioChanged == "no" && $shortaudioChanged == "yes"){
                $query = "update books set cat_id='$cat_id',book_name='$book_title',book_desc='$book_desc',
                book_author='$book_author', book_narrator='$book_narrator',play_time='$finalDuration',
                list_price='$org_price',discounted_price='$dis_price', book_status='$book_status',
                short_audio_file='$book_short_audio',front_look='$image_name' where book_id = '$book_id'";
            }
            else if($imageChanged == "no" && $audioChanged == "yes" && $shortaudioChanged == "no"){
                $query = "update books set cat_id='$cat_id',book_name='$book_title',book_desc='$book_desc',
                book_author='$book_author', book_narrator='$book_narrator',play_time='$finalDuration',
                list_price='$org_price',discounted_price='$dis_price',discount_id='$discount_on_book', book_status='$book_status',
                audio_file='$book_audio' where book_id = '$book_id'";
            }
            else if($imageChanged == "no" && $audioChanged == "yes" && $shortaudioChanged == "yes"){
                $query = "update books set cat_id='$cat_id',book_name='$book_title',book_desc='$book_desc',
                book_author='$book_author', book_narrator='$book_narrator',play_time='$finalDuration',
                list_price='$org_price',discounted_price='$dis_price',discount_id='$discount_on_book', book_status='$book_status',
                audio_file='$book_audio',short_audio_file='$book_short_audio' where book_id = '$book_id'";
            }
            else if($imageChanged == "no" && $audioChanged == "no" && $shortaudioChanged == "yes"){
                $query = "update books set cat_id='$cat_id',book_name='$book_title',book_desc='$book_desc',
                book_author='$book_author', book_narrator='$book_narrator',play_time='$finalDuration',
                list_price='$org_price',discounted_price='$dis_price',discount_id='$discount_on_book', book_status='$book_status',
                short_audio_file='$book_short_audio' where book_id = '$book_id'";
            }
            else {
                $query = "update books set cat_id='$cat_id',book_name='$book_title',book_desc='$book_desc',book_author='$book_author',
                book_narrator='$book_narrator',list_price='$org_price',discounted_price='$dis_price',discount_id='$discount_on_book',
                book_status='$book_status' where book_id = '$book_id'";
            }
            $result = mysqli_query($link, $query);
            if ($result) {
                $this->response[STATUS] = Success;
                $this->response[MESSAGE] = "Book Detail Updated SuccessFully";
                $this->response['bookId'] = $book_id;
            } else {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        } else {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
        }
        return $this->response;
    }
    public function checkBookExistence($book_title, $cat_id)
    {
        $link = $this->link->connect();
        if ($link) {
            $query = "select * from books where cat_id = '$cat_id' and book_name = '$book_title'";
            $result = mysqli_query($link, $query);
            if ($result) {
                $num = mysqli_num_rows($result);
                if ($num > 0) {
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = "Book Already Exist Please Enter Diffrent Name";
                    $row = mysqli_fetch_array($result);
                    $this->response['bookId'] = $row['book_id'];
                } else {
                    $this->response[STATUS] = Success;
                    $this->response[MESSAGE] = "Valid Name";
                }
            } else {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        } else {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
        }
        return $this->response;
    }
    public function getParticularBookData($bookId)
    {
        $link = $this->link->connect();
        if($link) {
            $query="select * from books where book_id='$bookId'";
            $result = mysqli_query($link,$query);
            if($result)
            {
                $num = mysqli_num_rows($result);
                if($num>0) {
                    $bookData = mysqli_fetch_assoc($result);
                    $this->response[STATUS] = Success;
                    $this->response[MESSAGE] = "Book Exist";
                    $this->response['bookData'] = $bookData;
                    $cat_id = $bookData['cat_id'];
                    $cat_data = $this->category->getParticularCatData($cat_id);
                    $cat_data = $cat_data['catData'];
                    $this->response['Category'] = $cat_data['cat_name'];
                }
                else{
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = "Invalid Book Identification";
                }
            }
            else
            {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else
        {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
        }
        return $this->response;
    }
    public function getAllBooks() {
        $catArray = array();
        $link = $this->link->connect();
        if($link) {
            $query="select * from books list_price!='0' order by book_id DESC";
            $result = mysqli_query($link,$query);
            if($result)
            {
                $num = mysqli_num_rows($result);
                if($num>0) {
                    while($bookData = mysqli_fetch_array($result)) {
                        $bookArray[]=array(
                            "book_id"=>$bookData['book_id'],
                            "cat_id"=>$bookData['cat_id'],
                            "book_name"=>$bookData['book_name'],
                            "book_desc"=>$bookData['book_desc'],
                            "book_author"=>$bookData['book_author'],
                            "book_narrator"=>$bookData['book_narrator'],
                            "play_time"=>$bookData['play_time'],
                            "list_price"=>$bookData['list_price'],
                            "discount_id"=>$bookData['discount_id'],
                            "book_status"=>$bookData['book_status']
                        );
                    }
                    $this->response[STATUS] = Success;
                    $this->response[MESSAGE] = "Data Found";
                    $this->response['data'] = $bookArray;
                }
                else{
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = "No Books Found";
                }
            }
            else
            {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else
        {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
        }
        return $this->response;
    }

    public function getAllFreeBooks() {
        $bookArray = array();
        $link = $this->link->connect();
        if($link) {
            $query="select * from books,categories where books.cat_id = categories.cat_id and list_price = '0' order by books.book_id DESC";
            $result = mysqli_query($link,$query);
            if($result)
            {
                $num = mysqli_num_rows($result);
                if($num>0) {
                    while($bookData = mysqli_fetch_array($result)) {
                        $bookArray[]=array(
                            "book_id"=>$bookData['book_id'],
                            "cat_id"=>$bookData['cat_id'],
                            "cat_name"=>$bookData['cat_name'],
                            "book_name"=>$bookData['book_name'],
                            "book_desc"=>$bookData['book_desc'],
                            "book_author"=>$bookData['book_author'],
                            "book_narrator"=>$bookData['book_narrator'],
                            "play_time"=>$bookData['play_time'],
                            "list_price"=>$bookData['list_price'],
                            "discounted_price"=>$bookData['discounted_price'],
                            "discount_id"=>$bookData['discount_id'],
                            "book_status"=>$bookData['book_status'],
                            "audio_file"=>$bookData['audio_file'],
                            "short_audio_file"=>$bookData['short_audio_file'],
                            "front_look"=>$bookData['front_look'],
                            "average_rate"=>$bookData['average_rate']
                        );
                    }
                    $this->response[STATUS] = Success;
                    $this->response[MESSAGE] = "Data Found";
                    $this->response['data'] = $bookArray;
                }
                else{
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = "No Books Found";
                }
            }
            else
            {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else
        {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
        }
        return $this->response;
    }

    public function getCategoryAllBooks($cat_id,$limit)
    {
        $bookArray = array();
        $link = $this->link->connect();
        if($link) {
            if($limit != 0) {
                $query = "select * from books where cat_id = '$cat_id' and list_price!='0' order by book_id DESC limit $limit";
            }else{
                $query = "select * from books where cat_id = '$cat_id' and list_price!='0' order by book_id DESC";
            }
            $result = mysqli_query($link,$query);
            if($result)
            {
                $num = mysqli_num_rows($result);
                if($num>0) {
                    while($bookData = mysqli_fetch_assoc($result)) {
                        $cat_id = $bookData['cat_id'];
                        $temp = $this->category->getParticularCatData($cat_id);
                        $cat_name = "";
                        if($temp[STATUS] == Success){
                            $cat_name = $temp['catData']['cat_name'];
                        }
                        $bookArray[]=array(
                            "book_id"=>$bookData['book_id'],
                            "cat_id"=>$bookData['cat_id'],
                            "cat_name"=>$cat_name,
                            "book_name"=>$bookData['book_name'],
                            "book_desc"=>$bookData['book_desc'],
                            "book_author"=>$bookData['book_author'],
                            "book_narrator"=>$bookData['book_narrator'],
                            "play_time"=>$bookData['play_time'],
                            "list_price"=>$bookData['list_price'],
                            "discounted_price"=>$bookData['discounted_price'],
                            "discount_id"=>$bookData['discount_id'],
                            "book_status"=>$bookData['book_status'],
                            "audio_file"=>$bookData['audio_file'],
                            "short_audio_file"=>$bookData['short_audio_file'],
                            "front_look"=>$bookData['front_look'],
                            "average_rate"=>$bookData['average_rate']
                        );
                    }
                    $this->response[STATUS] = Success;
                    $this->response[MESSAGE] = "Data Found";
                    $this->response['bookData'] = $bookArray;
                }
                else{
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = "No Books Found";
                }
            }
            else
            {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else
        {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
        }
        return $this->response;
    }
    public function getTopRatedBooks($limit)
    {
        $bookArray = array();
        $link = $this->link->connect();
        if($link) {
            if($limit != 0){
                $query="select * from books order by average_rate DESC limit $limit";
            }else{
                $query="select * from books order by average_rate DESC";
            }
            $result = mysqli_query($link,$query);
            if($result)
            {
                $num = mysqli_num_rows($result);
                if($num>0) {
                    while($bookData = mysqli_fetch_assoc($result)) {
                        $cat_id = $bookData['cat_id'];
                        $temp = $this->category->getParticularCatData($cat_id);
                        $cat_name = "";
                        if($temp[STATUS] == Success){
                            $cat_name = $temp['catData']['cat_name'];
                        }
                        $bookArray[]=array(
                            "book_id"=>$bookData['book_id'],
                            "cat_id"=>$bookData['cat_id'],
                            "cat_name"=>$cat_name,
                            "book_name"=>$bookData['book_name'],
                            "book_desc"=>$bookData['book_desc'],
                            "book_author"=>$bookData['book_author'],
                            "book_narrator"=>$bookData['book_narrator'],
                            "play_time"=>$bookData['play_time'],
                            "list_price"=>$bookData['list_price'],
                            "discounted_price"=>$bookData['discounted_price'],
                            "discount_id"=>$bookData['discount_id'],
                            "book_status"=>$bookData['book_status'],
                            "audio_file"=>$bookData['audio_file'],
                            "short_audio_file"=>$bookData['short_audio_file'],
                            "front_look"=>$bookData['front_look'],
                            "average_rate"=>$bookData['average_rate']
                        );
                    }
                    $this->response[STATUS] = Success;
                    $this->response[MESSAGE] = "Data Found";
                    $this->response['bookData'] = $bookArray;
                }
                else{
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = "No Books Found";
                }
            }
            else
            {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else
        {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
        }
        return $this->response;
    }
    public function getBookCount($cat_id)
    {
        $link = $this->link->connect();
        if($link) {
            $query="select * from books where cat_id = '$cat_id'";
            $result = mysqli_query($link,$query);
            if($result)
            {
                $num = mysqli_num_rows($result);
                $this->response[STATUS] = Success;
                $this->response[MESSAGE] = "Count Exist";
                $this->response["Count"] = $num;
            }
            else
            {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
                $this->response["Count"] = 0;
            }
        }
        else
        {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
            $this->response["Count"] = 0;
        }
        return $this->response;
    }
    public function statusChange($book_id,$value){
        $link = $this->link->connect();
        if($link) {
            $query = "select * from books where book_id='$book_id'";
            $result = mysqli_query($link, $query);
            if ($result) {
                $num = mysqli_num_rows($result);
                if ($num > 0) {
                    $update = mysqli_query($link, "UPDATE books SET book_status='$value' WHERE book_id='$book_id'");
                    if ($update) {
                        $this->response[STATUS] = Success;
                        $this->response[MESSAGE] = "Status Has Been Changed Successfully";
                    } else {
                        $this->response[STATUS] = Error;
                        $this->response[MESSAGE] = $this->link->sqlError();
                    }
                } else {
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = "UnAuthorized Access";
                }
            } else {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else{
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = "Connection Error";
        }
        return $this->response;
    }
    public function deleteBook($book_id){
        $link = $this->link->connect();
        if($link) {
            $query = "select * from books where book_id='$book_id'";
            $result = mysqli_query($link, $query);
            if ($result) {
                $num = mysqli_num_rows($result);
                if ($num > 0) {
                    $update = mysqli_query($link, "delete from books WHERE book_id='$book_id'");
                    if ($update) {
                        $row = mysqli_fetch_array($result);
                        unlink("Files/images/".$row['front_look']);
                        unlink("Files/audio/".$row['audio_file']);
                        $this->response[STATUS] = Success;
                        $this->response[MESSAGE] = "Book Has Been Deleted Successfully";
                    } else {
                        $this->response[STATUS] = Error;
                        $this->response[MESSAGE] = $this->link->sqlError();
                    }
                } else {
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = "UnAuthorized Access";
                }
            } else {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else{
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = "Connection Error";
        }
        return $this->response;
    }
    public function getRatesAndReviews($bookId)
    {
        $link = $this->link->connect();
        if($link) {
            $query="select * from book_rating where book_id='$bookId'";
            $result = mysqli_query($link,$query);
            if($result)
            {
                $num = mysqli_num_rows($result);
                if($num>0) {
                    $userRate = 0.0;
                    while($rows = mysqli_fetch_array($result)){
                        $userRate = ((float)$userRate+(float)$rows['user_rate']);
                        $user_id = $rows['user_id'];
                        $user_data = $this->userClass->getParticularUserData($user_id);
                        $user_data =$user_data['UserData'];
                        $rateArray[] =array(
                            "rating_id"=>$rows['rating_id'],
                            "user_rate"=>$rows['user_rate'],
                            "user_review"=>$rows['user_review'],
                            "user_name"=>$user_data['user_name'],
                            "user_image"=>$user_data['user_profile']
                        );
                    }
                    $userRate = $userRate/$num;
                    $this->response[STATUS] = Success;
                    $this->response[MESSAGE] = "Book Exist";
                    $this->response['userRate'] = $userRate;
                    $this->response['bookData'] = $rateArray;
                }
                else{
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = "Invalid Book Identification";
                }
            }
            else
            {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else
        {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
        }
        return $this->response;
    }

    function searchBooks($searchTxt,$searchType) {
         $link = $this->link->connect();
         $data = array();
         if($link) {
             $query = "select * from books,categories where (book_name LIKE '%" . $searchTxt . "%' and books.cat_id = categories.cat_id";

             if ($searchType == 'title') {
                 $query = "select * from books,categories where book_name LIKE '%" . $searchTxt . "%' and books.cat_id = categories.cat_id";
             } else if ($searchType == 'author') {
                 $query = "select * from books,categories where book_author LIKE '%" . $searchTxt . "%' and books.cat_id = categories.cat_id";
             } else if ($searchType == 'narrator') {
                 $query = "select * from books,categories where book_narrator LIKE '%" . $searchTxt . "%' and books.cat_id = categories.cat_id";
             }
             if ($searchType != 'category') {
                 $result = mysqli_query($link, $query);
                 if ($result) {
                     $num = mysqli_num_rows($result);
                     if ($num > 0) {
                         while ($bookData = mysqli_fetch_array($result)) {

                             $data[] = array("book_id" => $bookData['book_id'],
                                 "cat_id" => $bookData['cat_id'],
                                 "cat_name" => $bookData['cat_name'],
                                 "book_name" => $bookData['book_name'],
                                 "book_desc" => $bookData['book_desc'],
                                 "book_author" => $bookData['book_author'],
                                 "book_narrator" => $bookData['book_narrator'],
                                 "play_time" => $bookData['play_time'],
                                 "list_price" => $bookData['list_price'],
                                 "discounted_price" => $bookData['discounted_price'],
                                 "discount_id" => $bookData['discount_id'],
                                 "book_status" => $bookData['book_status'],
                                 "audio_file" => $bookData['audio_file'],
                                 "short_audio_file" => $bookData['short_audio_file'],
                                 "front_look" => $bookData['front_look'],
                                 "average_rate" => $bookData['average_rate']);

                         }
                         $this->response[STATUS] = Success;
                         $this->response[MESSAGE] = "Data found successfully";
                         $this->response["data"] = $data;

                     } else {
                         $this->response[STATUS] = Error;
                         $this->response[MESSAGE] = "Unable to get data";
                     }
                 }
             } else {
                 $query = "select * from categories where cat_name like '%" . $searchTxt . "%' ";
                 $result = mysqli_query($link, $query);
                 $temp = array();
                 if ($result) {
                     while ($rows = mysqli_fetch_array($result)) {
                         $temp[] = $rows['cat_id'];
                     }
                     $sub_str = 'where ';
                     for ($i = 0; $i < count($temp); $i++) {
                         if ($i == count($temp) - 1) {
                             $sub_str = $sub_str . " cat_id='" . $temp[$i] . "'";
                         } else {
                             $sub_str = $sub_str . " cat_id='" . $temp[$i] . "' or";
                         }
                     }
                     $num1 = 0;

                     if (count($temp) > 0) {
                         $query = "select * from books " . $sub_str;
                         $result = mysqli_query($link, $query);
                         if ($result) {
                             $num1 = mysqli_num_rows($result);

                             while ($bookData = mysqli_fetch_array($result)) {


                                 $data[] = array("book_id" => $bookData['book_id'],
                                     "cat_id" => $bookData['cat_id'],
                                     "cat_name" => $bookData['cat_name'],
                                     "book_name" => $bookData['book_name'],
                                     "book_desc" => $bookData['book_desc'],
                                     "book_author" => $bookData['book_author'],
                                     "book_narrator" => $bookData['book_narrator'],
                                     "play_time" => $bookData['play_time'],
                                     "list_price" => $bookData['list_price'],
                                     "discounted_price" => $bookData['discounted_price'],
                                     "discount_id" => $bookData['discount_id'],
                                     "book_status" => $bookData['book_status'],
                                     "audio_file" => $bookData['audio_file'],
                                     "short_audio_file" => $bookData['short_audio_file'],
                                     "front_look" => $bookData['front_look'],
                                     "average_rate" => $bookData['average_rate']);

                             }

                         }
                     }
                     if ($num1 == 0) {
                         $this->response[STATUS] = Error;
                         $this->response[MESSAGE] = "Unable to get data";
                     } else {
                         $this->response[STATUS] = Success;
                         $this->response[MESSAGE] = "Data found successfully";
                         $this->response["data"] = $data;

                     }
                 } else {
                     $this->response[STATUS] = Error;
                     $this->response[MESSAGE] = "No data found yet";

                 }

             }
         }
         else
             {
                 $this->response[STATUS] = Error;
                 $this->response[MESSAGE] = $this->link->sqlError();
             }

         return $this->response;
    }
    function totalBooks(){
        $link = $this->link->connect();
        if($link) {
            $query = "select * from books";
            $result = mysqli_query($link, $query);
            if ($result) {
                $num = mysqli_num_rows($result);
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
                $this->response['Count'] = $num;
            }
            else{
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else{
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
        }
        return $this->response;
    }
    function storeRecent($book_id,$user_id){
        $link = $this->link->connect();
        if($link) {
            $query = "select * from recentviewed where book_id = '$book_id' 
            and user_id = '$user_id'";
            $result = mysqli_query($link, $query);
            if ($result) {
                $num = mysqli_num_rows($result);
                if($num>0){
                    $query = "update recentviewed set last_seen = '$this->currentDateTimeStamp'
                    where book_id = '$book_id' and user_id = '$user_id'";
                    $result = mysqli_query($link,$query);
                    if($result){
                        $this->response[STATUS] = Success;
                        $this->response[MESSAGE] = "Book Added to Recent";
                    }else{
                        $this->response[STATUS] = Error;
                        $this->response[MESSAGE] = $this->link->sqlError();
                    }
                }else{
                    $query = "insert into recentviewed (book_id,user_id,last_seen) values('$book_id',
                    '$user_id','$this->currentDateTimeStamp')";
                    $result = mysqli_query($link,$query);
                    if($result){
                        $this->response[STATUS] = Success;
                        $this->response[MESSAGE] = "Book Added to Recent";
                    }else{
                        $this->response[STATUS] = Error;
                        $this->response[MESSAGE] = $this->link->sqlError();
                    }
                }
            }
            else{
                echo "ok";
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else{
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
        }
        return $this->response;
    }
    function getRecent($user_id) {
        $link = $this->link->connect();
        if($link) {
            $query = "select * from recentviewed where user_id = '$user_id'";
            $result = mysqli_query($link, $query);
            if ($result) {
                $num = mysqli_num_rows($result);
                if($num>0){
                    $bookData=array();
                    while($rows = mysqli_fetch_assoc($result)){
                        $book_id = $rows['book_id'];
                        $bookresp = $this->getParticularBookData($book_id);
                        $bookData[] = $bookresp['bookData'];
                    }
                    $this->response[STATUS] = Success;
                    $this->response[MESSAGE] = datafound;
                    $this->response['bookData'] = $bookData;
                }else{
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = "No Recent Item Found";
                }
            }
            else{
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else{
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
        }
        return $this->response;
    }

    function getRecentlyViewed($user_id) {
        $items = array();
        $link = $this->link->connect();
        if($link){
            $query  = "select * from recentviewed where user_id='$user_id' order by _id desc limit 1";
            $result = mysqli_query($link,$query);
            if($result) {
                $num = mysqli_num_rows($result);
                if($num>0) {
                    $rows = mysqli_fetch_assoc($result);
                    $book_id = $rows['book_id'];
                    $items = $this->getTypeData('category',$items,$book_id);
                    if(count($items) == 0) {
                        $this->response[STATUS] = Error;
                        $this->response[MESSAGE] = "Book data not found";
                        $this->response['bookData'] = $items;

                    }
                    else{
                        $this->response[STATUS] = Success;
                        $this->response[MESSAGE] = "Book data found";
                        $this->response['bookData'] = $items;
//                        print_r($items);

                    }

                }
                else{
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = "Book data not found";
                }
            }
            else{
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = "Book data not found";
            }
        }
        else{
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
        }
        return $this->response;
    }

    function getTypeData($type,$arr,$book_id) {
        $limit = 10;
        $bookData = $this->getParticularBookData($book_id);
        $link = $this->link->connect();

        switch ($type) {
            case 'category':
                $cat_id = $bookData['bookData']['cat_id'];
                 $query = "select * from books where cat_id ='$cat_id' and book_id!='$book_id'";
                 $result = mysqli_query($link,$query);

                 while($rows = mysqli_fetch_array($result)) {
                   $arr[] = array('book_id'=>$rows['book_id'],
                       'book_name'=>$rows['book_name'],
                       'book_desc'=>$rows['book_desc'],
                       'book_author'=>$rows['book_author'],
                       'book_narrator'=>$rows['book_narrator'],
                       'play_time'=>$rows['play_time'],
                       'list_price'=>$rows['list_price'],
                       'discounted_price'=>$rows['discounted_price'],
                       'audio_file'=>$rows['audio_file'],
                       'short_audio_file'=>$rows['short_audio_file'],
                       'front_look'=>$rows['front_look'],
                       'average_rate'=>$rows['average_rate']);
                 }
                 if(count($arr)>=$limit) {
                     return $arr;
                 }
                 return $this->getTypeData('rating',$arr,$book_id);
                break;

            case 'rating':
                $average_rate = $bookData['bookData']['average_rate'];
                $query = "select * from books where average_rate ='$average_rate' and book_id!='$book_id'";
                $result = mysqli_query($link,$query);

                while($rows = mysqli_fetch_array($result)) {
                    if(!$this->isBookExists($arr,$rows['book_id']))
                       $arr[] = array('book_id'=>$rows['book_id'],
                           'book_name'=>$rows['book_name'],
                           'book_desc'=>$rows['book_desc'],
                           'book_author'=>$rows['book_author'],
                           'book_narrator'=>$rows['book_narrator'],
                           'play_time'=>$rows['play_time'],
                           'list_price'=>$rows['list_price'],
                           'discounted_price'=>$rows['discounted_price'],
                           'audio_file'=>$rows['audio_file'],
                           'short_audio_file'=>$rows['short_audio_file'],
                           'front_look'=>$rows['front_look'],
                           'average_rate'=>$rows['average_rate']);
                }
                if(count($arr)>=$limit) {
                    return $arr;
                }
                return $this->getTypeData('author',$arr,$book_id);

                break;
            case 'author':
                $author = $bookData['bookData']['book_author'];
                $query = "select * from books where book_author ='$author' and book_id!='$book_id'";
                $result = mysqli_query($link,$query);

                while($rows = mysqli_fetch_array($result)) {
                    if(!$this->isBookExists($arr,$rows['book_id']))
                    $arr[] = array('book_id'=>$rows['book_id'],
                        'book_name'=>$rows['book_name'],
                        'book_desc'=>$rows['book_desc'],
                        'book_author'=>$rows['book_author'],
                        'book_narrator'=>$rows['book_narrator'],
                        'play_time'=>$rows['play_time'],
                        'list_price'=>$rows['list_price'],
                        'discounted_price'=>$rows['discounted_price'],
                        'audio_file'=>$rows['audio_file'],
                        'short_audio_file'=>$rows['short_audio_file'],
                        'front_look'=>$rows['front_look'],
                        'average_rate'=>$rows['average_rate']);
                }
                if(count($arr)>=$limit) {
                    return $arr;
                }
                return $this->getTypeData('narrator',$arr,$book_id);

                break;
            case 'narrator':
                $book_narrator = $bookData['bookData']['book_narrator'];
                $query = "select * from books where book_narrator ='$book_narrator' and book_id!='$book_id'";
                $result = mysqli_query($link,$query);

                while($rows = mysqli_fetch_array($result)) {
                    if(!$this->isBookExists($arr,$rows['book_id']))
                        $arr[] = array('book_id'=>$rows['book_id'],
                            'book_name'=>$rows['book_name'],
                            'book_desc'=>$rows['book_desc'],
                            'book_author'=>$rows['book_author'],
                            'book_narrator'=>$rows['book_narrator'],
                            'play_time'=>$rows['play_time'],
                            'list_price'=>$rows['list_price'],
                            'discounted_price'=>$rows['discounted_price'],
                            'audio_file'=>$rows['audio_file'],
                            'short_audio_file'=>$rows['short_audio_file'],
                            'front_look'=>$rows['front_look'],
                            'average_rate'=>$rows['average_rate']);
                }

                break;

        }
        return $arr;
    }

    function isBookExists($bk_arr,$bk_id) {
        $isFound = false;
        for($i=0;$i<count($bk_arr);$i++) {
            if($bk_arr[$i]['book_id'] == $bk_id) {
                $isFound = true;
                break;
            }
        }
        return $isFound;
    }
    function purchaseBook($user_id, $book_id, $amount_paid, $payment_status){
        $link = $this->link->connect();
        if($link) {
            $query = "select * from purchase_book where user_id = '$user_id' and book_id = '$book_id'";
            $result = mysqli_query($link, $query);
            if ($result) {
                $num = mysqli_num_rows($result);
                if($num>0){
                    $row = mysqli_fetch_array($result);
                    if($row['payment_status']=="Success"){
                        $this->response[STATUS] = Error;
                        $this->response[MESSAGE] = "Book has been Already Purchased";
                    }
                    else{
                        $query = mysqli_query($link, "UPDATE purchase_book SET user_id='$user_id',
                            book_id='$book_id',amount_paid='$amount_paid', payment_status='$payment_status'
                            WHERE book_id='$book_id' and user_id = '$user_id'");
                        if ($query) {
                                $this->getbookPurchaseDetail($user_id,$book_id);
                        }
                        else {
                            $this->response[STATUS] = Error;
                            $this->response[MESSAGE] = $this->link->sqlError();
                        }
                    }
                }
                else{
                    $query = "insert into purchase_book (user_id, book_id, amount_paid, payment_status) VALUES 
                    ('$user_id', '$book_id', '$amount_paid', '$payment_status')";
                    $result = mysqli_query($link, $query);
                    if($result){
                        // success
                        $this->response[STATUS] = Success;
                    }
                    else{
                        $this->response[STATUS] = Error;
                        $this->response[MESSAGE] = $this->link->sqlError();
                    }
                }
            }
            else{
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else{
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
        }
        return $this->response;
    }
    function getbookPurchaseDetail($user_id, $book_id){
        $link = $this->link->connect();
        if($link) {
            $query = "select * from purchase_book where user_id = '$user_id' and book_id = '$book_id'";
            $result = mysqli_query($link, $query);
            if ($result) {
                $num = mysqli_num_rows($result);
                if ($num > 0) {
                    $row = mysqli_fetch_assoc($result);
                    if ($row['payment_status'] == "Success") {
                        $this->response[STATUS] = Success;
                        $this->response[MESSAGE] = "Book has been Purchased";
                    }
                    else {
                        $this->response[STATUS] = Error;
                        $this->response[MESSAGE] = "Book has not been Purchased";
                    }
                }
                else{
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = "Book has not been Purchased";
                }
            }
            else{
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else{
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
        }
        return $this->response;
    }
    public function addRatesAndReviews($bookId,$userId,$rate,$review){
        $link = $this->link->connect();
        if($link) {
            $purchaseResponse = $this->getbookPurchaseDetail($userId, $bookId);
            if ($purchaseResponse[STATUS] == Error) {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = "Please Purchase the Book First";
            } else {
                $query = "select * from book_rating where book_id = '$bookId' and user_id = '$userId'";
                $result = mysqli_query($link,$query);
                if($result) {
                    $num = mysqli_num_rows($result);
                    if ($num > 0) {
                        //user already rated
                        $query = "update book_rating set user_rate = '$rate',user_review = '$review' where 
                        book_id = '$bookId' and user_id = '$userId'";
                        $result = mysqli_query($link,$query);
                        if($result){
                            $this->response[STATUS] = Success;
                            $this->response[MESSAGE] = "Rating Added Successfully";
                        }else{
                            $this->response[STATUS] = Error;
                            $this->response[MESSAGE] = $this->link->sqlError();
                        }
                    } else {
                        //new user Rating
                        $query = "insert into book_rating (user_rate,user_review,user_id,book_id) values('$rate',
                        '$review','$userId','$bookId')";
                        $result = mysqli_query($link,$query);
                        if($result){
                            $this->response[STATUS] = Success;
                            $this->response[MESSAGE] = "Rating Added Successfully";
                        }else{
                            $this->response[STATUS] = Error;
                            $this->response[MESSAGE] = $this->link->sqlError();
                        }
                    }
                    $this->updateAverageRating($bookId);
                } else {
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = $this->link->sqlError();
                }
            }
        }
        else{
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
        }
        return $this->response;
    }

    public function updateAverageRating($bookId){
        $link = $this->link->connect();
        if($link) {
            $query = "SELECT AVG(user_rate) avg_rating FROM book_rating WHERE book_id = '$bookId'";
            $result = mysqli_query($link,$query);
            if($result) {
                $rows = mysqli_fetch_array($result);
                $avg_rate = $rows['avg_rating'];
                $query = "update books set average_rate = '$avg_rate' where book_id = '$bookId'";
                $result = mysqli_query($link,$query);
                if($result){
                    $this->response[STATUS] = Success;
                    $this->response[MESSAGE] = "Rating Added Successfully";
                }else{
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = $this->link->sqlError();
                }
            } else {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
        }
        return $this->response;
    }

    public function createSubList($userId,$json) {
        $link = $this->link->connect();
        mysqli_set_charset($link, "utf8");
        if($link) {
           $query = "select * from users,membership where users.active_plan = membership.plan_id and membership.plan_price!=0 and users.user_id='$userId'";
           $result = mysqli_query($link,$query);
           $num = mysqli_num_rows($result);
           if($num>0) {
//               var_dump($json);
//               $json = str_replace('detail','"detail"',$json);
               $json = json_decode($json,true);
//               print_r($json);

//              echo $json;
              $title = $json['title'];
              $detail = $json['detail'];
//              echo $title;
              $query = "insert into book_list(list_name,list_userid)values('$title','$userId')";
              $result = mysqli_query($link,$query);
              if($result) {
                  $id_ = mysqli_insert_id($link);
                  for($i=0;$i<count($detail);$i++) {
                      $bookId = $detail[$i];
                      $query = "insert into book_list_detail(det_list_id,det_book_id)values('$id_','$bookId')";
                      $result = mysqli_query($link,$query);
                       if($i == count($detail)-1 && $result) {
                           $this->response[STATUS] = Success;
                           $this->response["data"] = $id_;
                           $this->response[MESSAGE] = "Book list created successfully";
                       }
                  }
              }
           }
           else{
               $this->response[STATUS] = Error;
               $this->response[MESSAGE] = "This facility available for only premimum user";
           }
        }
        else{
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();

        }
        return $this->response;
    }

    public function getSubList($userId) {
        $data = array();
        $link = $this->link->connect();
         if($link) {
             $query = "select * from book_list where list_userid='$userId'";
             $result = mysqli_query($link,$query);
             if($result) {
                $num = mysqli_num_rows($result);
                if($num>0) {
                  $counter = 0;
                  while($rows = mysqli_fetch_array($result)) {
                      $listId = $rows['list_id'];
                      $response = $this->getParticularListDetail($listId);
                      if($response[STATUS] == Success) {
                          $data[] = array('list_id'=>$listId,
                              "list_name"=>$rows['list_name'],
                              "list_created"=>$rows['list_created_at'],
                              "list_detail"=>$response["data"]["list_detail"]);

                      }
                      $counter++;
                  }
                    $this->response[STATUS] = Success;
                    $this->response[MESSAGE] = "Data found successfully";
                    $this->response['data'] = $data;


                }
                else{
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = "Data not found yet";
                }
             }
             else{
                 $this->response[STATUS] = Error;
                 $this->response[MESSAGE] = "Unable to get data ! ! ! !";
             }
         }
         else{
             $this->response[STATUS] = Error;
             $this->response[MESSAGE] = $this->link->sqlError();
         }
         return $this->response;
    }

    public function getParticularListDetail($listId) {
        $link = $this->link->connect();
        $data = array();


        $query = "select * from book_list where list_id='$listId'";
        $result = mysqli_query($link,$query);
        if($result) {
            $num = mysqli_num_rows($result);
            if($num>0){
                $bookArray = array();
                while($rows = mysqli_fetch_array($result)) {

                    $query = "select * from books,book_list_detail where books.book_id = book_list_detail.det_book_id and book_list_detail.det_list_id='$listId'";
//                    echo $query;
                    $result = mysqli_query($link,$query);

                    while($bookData = mysqli_fetch_array($result)) {
                        $bookArray[] = array(
                            "book_id" => $bookData['book_id'],
                            "cat_id" => $bookData['cat_id'],
                            "cat_name" => $bookData['cat_name'],
                            "book_name" => $bookData['book_name'],
                            "book_desc" => $bookData['book_desc'],
                            "book_author" => $bookData['book_author'],
                            "book_narrator" => $bookData['book_narrator'],
                            "play_time" => $bookData['play_time'],
                            "list_price" => $bookData['list_price'],
                            "discounted_price" => $bookData['discounted_price'],
                            "discount_id" => $bookData['discount_id'],
                            "book_status" => $bookData['book_status'],
                            "audio_file" => $bookData['audio_file'],
                            "short_audio_file" => $bookData['short_audio_file'],
                            "front_look" => $bookData['front_look'],
                            "average_rate" => $bookData['average_rate']);
                    }
                    if(count($bookArray)>0) {
                        $data = array('list_id'=>$listId,
                            "list_name"=>$rows['list_name'],
                            "list_created"=>$rows['list_created_at'],
                            "list_detail"=>$bookArray);

                    }


                }
                $this->response[STATUS] = Success;
                $this->response["data"] = $data;
                $this->response[MESSAGE] = "got list data";

            }
            else{
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else{
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();

        }

        return $this->response;
    }
    public function apiResponse($response)
    {
        header("Content-Type: application/json");
        echo json_encode($response);
    }
}