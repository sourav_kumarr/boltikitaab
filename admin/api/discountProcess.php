<?php
require_once ('Classes/DISCOUNT.php');
require_once ('Constants/functions.php');
require_once('Constants/configuration.php');
require_once('Constants/DbConfig.php');
$discountClass = new \Classes\DISCOUNT();
$requiredfields = array('type');
($response = RequiredFields($_POST, $requiredfields));
if($response['Status'] == 'Failure'){
    $discountClass->apiResponse($response);
    return false;
}
error_reporting(0);
$type = $_POST['type'];
if($type == "addCategory")
{
    $requiredfields = array('cat_name','cat_status');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $discountClass->apiResponse($response);
        return false;
    }
    $cat_name = trim($_POST['cat_name']);
    $cat_status = trim($_POST['cat_status']);
    ($response = $discountClass->checkCategoryExistence($cat_name));
    if($response[STATUS] == Success) {
        $response = $discountClass->addCategory($cat_name,$cat_status);
    }
    if($response[STATUS] == Error){
        $discountClass->apiResponse($response);
        return false;
    }
    $catId = $response['catId'];
    $temp = $discountClass->getParticularCatData($catId);
    $response['catData'] = $temp['catData'];
    $response[ImagesBaseURLKey] = ImagesBaseURL;
    unset($response['catId']);
    $discountClass->apiResponse($response);
}
else if($type == "editCategory")
{
    $requiredfields = array('cat_name','cat_status','cat_id');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $discountClass->apiResponse($response);
        return false;
    }
    $cat_name = trim($_POST['cat_name']);
    $cat_status = trim($_POST['cat_status']);
    $cat_id = trim($_POST['cat_id']);
    $imageChanged = trim($_POST['imageChanged']);
    $response = $discountClass->editCategory($cat_name,$cat_status,$cat_id,$imageChanged);
    if($response[STATUS] == Error){
        $discountClass->apiResponse($response);
        return false;
    }
    $catId = $response['catId'];
    $temp = $discountClass->getParticularCatData($catId);
    $response['catData'] = $temp['catData'];
    $response[ImagesBaseURLKey] = ImagesBaseURL;
    unset($response['catId']);
    $discountClass->apiResponse($response);
}
else if($type == "getCoupons")
{
    $response = $discountClass->getAllCoupons();
    $discountClass->apiResponse($response);
}
else if($type == "getCategory"){
    $requiredfields = array('cat_id');
    if(!RequiredFields($_REQUEST, $requiredfields)) {
        return false;
    }
    $cat_id = $_REQUEST['cat_id'];
    $response = $discountClass->getParticularCatData($cat_id);
    $discountClass->apiResponse($response);
}
else if($type == "statusChange")
{
    $requiredfields = array('cat_id','value');
    if(!RequiredFields($_REQUEST, $requiredfields)) {
        return false;
    }
    $value = $_REQUEST['value'];
    $cat_id = $_REQUEST['cat_id'];
    $response = $discountClass->statusChange($cat_id,$value);
    if($response[STATUS] == Error) {
        $discountClass->apiResponse($response);
        return false;
    }
    $discountClass->apiResponse($response);
}
else if($type == "deleteCategory")
{
    $requiredfields = array('cat_id');
    if(!RequiredFields($_REQUEST, $requiredfields)) {
        return false;
    }
    $cat_id = $_REQUEST['cat_id'];
    $response = $discountClass->deleteCategory($cat_id);
    if($response[STATUS] == Error) {
        $discountClass->apiResponse($response);
        return false;
    }
    $discountClass->apiResponse($response);
}
else{
    $response[STATUS] = Error;
    $response[MESSAGE] = "502 UnAuthorised Request";
    $discountClass->apiResponse($response);
}
?>