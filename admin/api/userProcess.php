<?php
require_once ('Classes/BOOKS.php');
require_once ('Classes/USERCLASS.php');
require_once ('Constants/functions.php');
require_once('Constants/configuration.php');
require_once('Constants/DbConfig.php');
$userClass = new \Classes\USERCLASS();

$requiredfields = array('type');
($response = RequiredFields($_POST, $requiredfields));
if($response['Status'] == 'Failure'){
    $userClass->apiResponse($response);
    return false;
}
$type = $_POST['type'];
if($type == "register")
{
    $requiredfields = array('email','socialType');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $userClass->apiResponse($response);
        return false;
    }
    $email = trim($_POST['email']);
    $socialType = trim($_POST['socialType']);
    $emailResponse = $userClass->checkEmailExistence($email);
    if($emailResponse[STATUS] == Error){
        $response[STATUS] = Success;
        $response[MESSAGE] = $emailResponse[MESSAGE];
        $user_id = $emailResponse['userId'];
        ($userResponse = $userClass->getParticularUserData($user_id));
        $response['UserData'] = $userResponse['UserData'];
    }else {
        $requiredfields = array('username','contactNumber','address');
        $response = RequiredFields($_POST, $requiredfields);
        if($response['Status'] == 'Failure'){
            $userClass->apiResponse($response);
            return false;
        }
        $username = trim($_POST['username']);
        $contactNumber = trim($_POST['contactNumber']);
        $address = trim($_POST['address']);
        $image_url = trim($_POST['image_url']);
        $response = $userClass->registerUser($username, $email, $contactNumber, $address, $socialType, $image_url);
    }
    $userClass->apiResponse($response);
}
else if($type == "getParticularUserData")
{
    $requiredfields = array('userId');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $userClass->apiResponse($response);
        return false;
    }
    $userId = $_POST['userId'];
    $response = $userClass->getParticularUserData($userId);
    $userClass->apiResponse($response);
}
else if($type == "statusChange")
{
    $requiredfields = array('user_id','value');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $userClass->apiResponse($response);
        return false;
    }
    $value = $_REQUEST['value'];
    $user_id = $_REQUEST['user_id'];
    $response = $userClass->statusChange($user_id,$value);
    if($response[STATUS] == Error) {
        $userClass->apiResponse($response);
        return false;
    }
    $userClass->apiResponse($response);
}
else if($type == "renewalStatusChange")
{
    $requiredfields = array('user_id','value');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $userClass->apiResponse($response);
        return false;
    }
    $value = $_REQUEST['value'];
    $user_id = $_REQUEST['user_id'];
    $response = $userClass->renewalStatusChange($user_id,$value);
    if($response[STATUS] == Error) {
        $userClass->apiResponse($response);
        return false;
    }
    $userClass->apiResponse($response);
}
else if($type == "deleteUser")
{
    $requiredfields = array('user_id');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $userClass->apiResponse($response);
        return false;
    }
    $user_id = $_REQUEST['user_id'];
    $response = $userClass->deleteUser($user_id);
    if($response[STATUS] == Error) {
        $userClass->apiResponse($response);
        return false;
    }
    $userClass->apiResponse($response);
}
else if($type == "updateUser")
{
    $requiredfields = array('userId','username','contactNumber','address');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $userClass->apiResponse($response);
        return false;
    }
    $userId = $_POST['userId'];
    $username = $_POST['username'];
    $contactNumber = $_POST['contactNumber'];
    $address = $_POST['address'];
    $response = $userClass->updateUser($userId,$username,$contactNumber,$address);
    if($response[STATUS] == Error){
        $userClass->apiResponse($response);
        return false;
    }
    $userId = $response['userId'];
    $temp = $userClass->getParticularUserData($userId);
    $response['UserData'] = $temp['UserData'];
    $response['ImagesBaseURL'] = ImagesBaseURL;
    unset($response['userId']);
    $userClass->apiResponse($response);
}
///////////////////////////////////////////////////*/
else if($type == "updatePlan")
{
    $requiredfields = array('userId','plan_id','renewal_type','auto_renewal');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $userClass->apiResponse($response);
        return false;
    }
    $userId = $_POST['userId'];
    $plan_id = $_POST['plan_id'];
    $renewal_type = $_POST['renewal_type'];
    $auto_renewal = $_POST['auto_renewal'];
    $response = $userClass->updatePlan($userId, $plan_id, $renewal_type, $auto_renewal);
    if($response['Status'] == 'Failure')
    {
        $userClass->apiResponse($response);
        return false;
    }
    $userId = $response['userId'];
    $temp = $userClass->getParticularUserData($userId);
    $response['UserData'] = $temp['UserData'];
    $response['ImagesBaseURL'] = ImagesBaseURL;
    unset($response['userId']);
    $userClass->apiResponse($response);
}
else if($type == 'cancelPlan') {
    $requiredfields = array('userId','auto_renewal');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $userClass->apiResponse($response);
        return false;
    }

    $userId = $_POST['userId'];
    $auto_renewal = $_POST['auto_renewal'];
    $response = $userClass->updateRenewel($userId,$auto_renewal);
    $userClass->apiResponse($response);
}
else if($type == "getPurchedBookList") {
    $requiredfields = array('user_id');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $userClass->apiResponse($response);
        return false;
    }
    $user_id = $_REQUEST['user_id'];
    ($response = $userClass->getPurchedBookList($user_id));
    if($response[STATUS] == Error) {
        $userClass->apiResponse($response);
        return false;
    }
    unset($response['Category']);
    $userClass->apiResponse($response);
}
else if($type == "updateSubscribe")
{
    $requiredfields = array('userId','subscribed');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $userClass->apiResponse($response);
        return false;
    }
    $userId = $_POST['userId'];
    $subscribed = $_POST['subscribed'];
    $subscribedFor = "";
    if($subscribed == "Yes"){
        $requiredfields = array('subscribedFor');
        $response = RequiredFields($_POST, $requiredfields);
        if($response['Status'] == 'Failure'){
            $userClass->apiResponse($response);
            return false;
        }
        $subscribedFor = $_POST['subscribedFor'];
    }
    $response = $userClass->updateSubscribe($userId, $subscribed, $subscribedFor);
    if($response['Status'] == 'Failure')
    {
        $userClass->apiResponse($response);
        return false;
    }
    $temp = $userClass->getParticularUserData($userId);
    $response['UserData'] = $temp['UserData'];
    $response['ImagesBaseURL'] = ImagesBaseURL;
    $userClass->apiResponse($response);
 }
else if($type == "updateRating")
{
    $requiredfields = array('userId','rateTxt','rating');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $userClass->apiResponse($response);
        return false;
    }
    $user_id = trim($_REQUEST['userId']);
    $rateTxt = trim($_REQUEST['rateTxt']);
    $rating = trim($_REQUEST['rating']);
    $response = $userClass->insertUserRating($user_id,$rating,$rateTxt);
    if($response['Status'] == 'Failure')
    {
        $userClass->apiResponse($response);
        return false;
    }
    $userClass->apiResponse($response);
}
else{
    $response[STATUS] = Error;
    $response[MESSAGE] = "502 UnAuthorised Request";
    $userClass->apiResponse($response);
}
?>