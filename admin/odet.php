<?php
include('header.php');
$order_id = $_REQUEST['_'];
echo "<input type='hidden' value=".$order_id." id='order_id' />";
?>
<style>
    .boxes{
        background: white;
        min-height:100px;
        border:1px solid #ddd;
        margin-bottom: 10px;
    }
    .Review{
        box-shadow: 3px 3px 2px #ccc;
        margin-bottom: 20px;
    }
</style>
<!-- page content -->
<div class="right_col" role="main">
    <!-- top tiles -->
    <div class="row" role="main">
        <div class="">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2 style="cursor:pointer" onclick="back()"><i class="fa fa-arrow-circle-left"></i> All Orders<small></small></h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li>
                                    <button onclick="window.location='api/excelProcess.php?dataType=particularOrder&order_id=<?php echo $order_id ?>'" class="btn btn-info btn-sm">Download Excel File</button>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <p class="text-muted font-13 m-b-30">
                                View Order Id "<span id="orderNumber"></span>" Detail
                            </p>
                        </div>
                    </div>
                    <div class="col-md-7 boxes">
                        <h2 style="text-align: center">Order Detail</h2>
                        <hr>
                        <div class="col-md-12">
                            <div class="col-md-8" style="padding: 0">
                                <div class="form-group">
                                    <label>User Name</label>
                                    <p id="userName"></p>
                                </div>
                                <div class="form-group">
                                    <label>Created On</label>
                                    <p id="order_created"></p>
                                </div>
                                <div class="form-group">
                                    <label>Order Id</label>
                                    <p id="orderNumber2"></p>
                                </div>
                                <div class="form-group">
                                    <label>Transaction Id</label>
                                    <p id="transactionId"></p>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <img src="" id="userImage" class="img-responsive img-thumbnail" />
                            </div>
                        </div>
                        <h4 style="text-align:center;">Order Contain Books</h4>
                        <hr>
                        <div class="col-md-12" style="height:205px;overflow-y: scroll">
                            <table class="table table-stripped" id="orderDetailTable">
                            </table>
                        </div>
                    </div>
                    <div class="col-md-5">
                        <div class="col-md-12 boxes">
                            <h2 style="text-align: center">Order Payment Detail</h2>
                            <hr>
                            <div class="col-md-12" style="text-align: center;margin-bottom: 20px">
                                <label >Order Amount</label>
                                <p style="font-size: 20px" id="amount"></p>
                                <p style="font-size: 20px">Payment <span id="payment_status">Success</span></p>
                                <label>Paid By PayPal </label>
                            </div>
                        </div>
                        <div class="col-md-12 boxes" style="max-height:360px;overflow-y: scroll" id="availBooks">
                            <h2 style="text-align: center">Available Books in Order</h2>
                            <hr>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /page content -->
<?php
include('footer.php');
?>
<script>
    var order_id = $("#order_id").val();
    var url = "api/orderProcess.php";
    $.post(url,{"type":"getOrder","order_id":order_id} ,function (data) {
        var status = data.Status;
        if (status == "Success"){
            var order = data.order;
            $("#orderNumber").html(order.order_number);
            $("#userName").html(order.order_user_name);
            $("#order_created").html(order.order_dated);
            $("#orderNumber2").html(order.order_number);
            $("#transactionId").html(order.transaction_id);
            $("#userImage").attr("src", 'images/img.png');
            if(order.user_profile != "") {
                $("#userImage").attr("src", order.user_profile);
            }
            $("#payment_status").html(order.payment_status);
            var amount = parseFloat(order.amount);
            amount = amount.toLocaleString();
            $("#amount").html("$"+amount+"/-");
            var orderDetail = order.order_detail;
            var booksDataDiv = "";
            var booksDataTable = "<tr><th>#</th><th>Book Name</th><th>Author</th><th>Narrator</th><th>Play Time</th></tr>";
            for(var i=0;i<orderDetail.length;i++) {
                var item_price = parseFloat(orderDetail[i].item_price);
                item_price = item_price.toLocaleString();
                booksDataDiv+="<div class='col-md-12 Review'><div class='col-md-2'><img src='api/Files/images/"+orderDetail[i].front_look+"' " +
                "class='img-responsive img-thumbnail' /></div><div class='col-md-8'><label>"+orderDetail[i].book_name+" " +
                "( <i onclick=playSong('"+orderDetail[i].audio_file+"') class='fa fa-play' style='color:green;cursor:pointer'>" +
                "</i> ) ( $"+item_price+" ) </label><p>"+orderDetail[i].book_desc.substr(0,30)+"...</p></div></div>";
                booksDataTable+="<tr><td>"+(i+1)+"</td><td>"+orderDetail[i].book_name+"</td><td>"+orderDetail[i].book_author+"</td>" +
                "<td>"+orderDetail[i].book_narrator+"</td><td>"+orderDetail[i].play_time+"</td></tr>";
            }
            $("#availBooks").append(booksDataDiv);
            $("#orderDetailTable").append(booksDataTable);
        }
        else{
            showMessage(data.Message,"red");
        }
    }).fail(function(){
        showMessage("Server Error!!! Please Try After Some Time","red")
    });
</script>
