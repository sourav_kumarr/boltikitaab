<?php
include('header.php');
include('api/Classes/CONNECT.php');
include('api/Constants/DbConfig.php');
include('api/Constants/configuration.php');
require_once('api/Classes/MEMBERSHIP.php');
$conn = new \Classes\CONNECT();
$membership = new \Classes\MEMBERSHIP();
?>
<!-- page content -->
<div class="right_col" role="main">
    <div class="row tile_count">
        <a href="users"><div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                <span class="count_top"><i class="fa fa-user"></i> Total Users</span>
                <div class="count" id="userCount"></div>
                <span class="count_bottom"><i class="green">Click </i>to Expand</span>
            </div></a>
        <a href="books"><div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                <span class="count_top"><i class="fa fa-volume-up"></i> Total Audio Books</span>
                <div class="count" id="booksCount"></div>
                <span class="count_bottom"><i class="green"></i> in All Categories</span>
            </div></a>
        <a href="index"><div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                <span class="count_top"><i class="fa fa-book"></i> Total Categories</span>
                <div class="count green" id="catCount"></div>
                <span class="count_bottom"><i class="green"></i> Click to Expand</span>
            </div></a>
        <a href="membership"><div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                <span class="count_top"><i class="fa fa-users"></i> MemberShip Types</span>
                <div class="count" id="membershipCount"></div>
                <span class="count_bottom"> Click to View</span>
            </div></a>
        <a href="discount"><div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                <span class="count_top"><i class="fa fa-cc-discover"></i> Discount Coupons</span>
                <div class="count" id="allCoupon"></div>
                <span class="count_bottom"><i class="green" id="activeCoupon"></i> is Still Active</span>
            </div></a>
        <a href="orders"><div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                <span class="count_top"><i class="fa fa-first-order"></i> Orders</span>
                <div class="count" id="orderCount"></div>
                <span class="count_bottom"><i class="green"></i>Click to Expand</span>
            </div></a>
    </div>
    <div class="">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>All Users <small></small></h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li>
                                <button onclick="window.location='api/excelProcess.php?dataType=allUsers'" class="btn btn-info btn-sm">Download Excel File</button>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <p class="text-muted font-13 m-b-30">
                            View the Details of All Users
                        </p>
                        <table id="datatable-buttons" class="table table-striped table-bordered">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Profile</th>
                                <th>User Name</th>
                                <th>E-Mail</th>
                                <th>Phone</th>
                                <th>Plan</th>
                                <th>Activation Date</th>
                                <th>Expiry Date</th>
                                <th>Auto Renew</th>
                                <th>Login</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $link = $conn->connect();
                            if ($link) {
                                $query = "select * from users order by user_id DESC";
                                $result = mysqli_query($link, $query);
                                if ($result) {
                                    $num = mysqli_num_rows($result);
                                    if ($num > 0) {
                                        $j = 0;
                                        while ($userData = mysqli_fetch_array($result)) {
                                            $j++;
                                            ?>
                                            <tr>
                                                <td data-title='#'><?php echo $j ?></td>
                                                <td data-title='Profile'>
                                                    <?php if($userData['user_profile'] == ""){
                                                        $userProfile = "img.png";
                                                    }else{
                                                        $userProfile = $userData['user_profile'];
                                                    }?>
                                                    <img src='api/Files/images/<?php echo $userProfile ?>'
                                                         style='height:35px' class='img-thumbnail'>
                                                </td>
                                                <td data-title='User Name'>
                                                    <!--<a href=' '--><!--udet?_=--><?php /*echo $userData['user_id'] */?><?php echo $userData['user_name'] ?><!--</a>-->
                                                </td>
                                                <td data-title='Email'><?php echo $userData['user_email'] ?></td>
                                                <td data-title='Phone'><?php echo $userData['user_contact'] ?></td>
                                                <td data-title='Plan'>
                                                <?php $plan_data = $membership->getParticularPlanData($userData['active_plan']);
                                                    $plan_data = $plan_data['planData'];
                                                    $plan_name = $plan_data['plan_name'];
                                                ?>
                                                <?php echo $plan_name ?></td>
                                                <td data-title='Activation Date'><?php echo date("d-M-Y",$userData['activation_date'] )?></td>
                                                <td data-title='Expiry Date'><?php echo date("d-M-Y",$userData['plan_expiry_date']) ?></td>
                                                <td class='buttonsTd' data-title='Auto Renew'>
                                                    <?php
                                                    $arbox = "";
                                                    if ($userData['auto_renewal'] == "1") {
                                                        $arbox  = "checked";
                                                    }
                                                    ?>
                                                    <input data-onstyle='info' class='renewalstatus' <?php echo $arbox ?>
                                                    data-toggle='toggle' value="<?php echo $userData['auto_renewal'] ?>"
                                                    id="<?php echo $userData['user_id']?>" data-size='mini' data-style='ios'
                                                    type='checkbox' />
                                                </td>
                                                <td class='buttonsTd' data-title='Login Access'>
                                                    <?php
                                                    $box = "";
                                                    if ($userData['user_status'] == "1") {
                                                        $box = "checked";
                                                    }
                                                    ?>
                                                    <input data-onstyle='info' class='userstatus' <?php echo $box ?>
                                                    data-toggle='toggle' value="<?php echo $userData['user_status'] ?>"
                                                    id="<?php echo $userData['user_id']?>" data-size='mini' data-style='ios'
                                                    type='checkbox' />
                                                </td>
                                                <td data-title='Action'>
                                                    <i class='fa fa-trash'
                                                       onclick=deleteUser('<?php echo $userData['user_id'] ?>')
                                                       style='color:#D05E61;cursor: pointer'/>
                                                </td>
                                            </tr>
                                            <?php
                                        }
                                    }
                                }
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /page content -->
<?php
include('footer.php');
?>
<script>
    $(document).ready(function () {
        $('#bookTable').DataTable({});
    });
</script>