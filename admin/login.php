<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Bolti Kitaab</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    <link href="css/custom.css" rel="stylesheet">
</head>
<style>
    .loader {
        height: 700px;
        background: rgba(255, 255, 255, 0.8);
        position: absolute;
        width: 100%;
        top: 0;
        z-index:999;
        display:none;
    }
    .loaderImg{
        height: 40px;
        margin-left: 49%;
        margin-top: 240px;
    }
</style>
<?php
if(isset($_REQUEST["_"])){
    if($_REQUEST["_"] == "cpd"){
        echo "<input type='hidden' value='cpd' id='cpd' />";
    }
}
?>
<body class="login">
<div class="loader">
    <img src="images/default.gif" class="loaderImg" />
</div>
<div class="login_wrapper">
    <div class="form login_form">
        <section class="login_content">
            <h1>Admin Login</h1>
            <div class="form-group">
                <input type="text" class="form-control" placeholder="Username" id="username"/>
            </div>
            <div class="form-group">
                <input type="password" class="form-control" placeholder="Password" id="password"/>
            </div>
            <div class="form-group">
                <a class="btn btn-default" onclick="loginNow()">Admin Log in</a>
                <a class="reset_pass" onclick="forgotPassword()">Forgot Password?</a>
            </div>
            <div class="clearfix"></div>
            <div class="separator">
                <div class="clearfix"></div>
                <br/>
                <div>
                    <h1 style="letter-spacing:0.5px"><i class="fa fa-book"></i> Bolti Kitaab</h1>
                    <p>©2017 All Rights Reserved. Bolti Kitaab, Privacy and Terms</p>
                </div>
            </div>
        </section>
    </div>
</div>
<div id="myModal" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Modal Header</h4>
            </div>
            <div class="modal-body">
                <p>Some text in the modal.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
</body>
</html>
<script src="js/jquery.min.js" ></script>
<script src="js/bootstrap.min.js" ></script>
<script src="js/myscript.js" ></script>
<script src="js/moment.min.js" ></script>
<script src="js/daterangepicker.js" ></script>
<script>
    function loginNow(){
        var userName = $("#username").val();
        var Password = $("#password").val();
        if(validateEmail(userName))
        {
            var url = "api/admin_login.php";
            $.post(url, {"type": "admin_login","email":userName,"password":Password}, function (data) {
                var status = data.Status;
                if (status == "Success") {
                    showMessage("Login Success !!! Please Wait.....", 'green');
                    setTimeout(function(){
                        window.location="index.php";
                    },1500);
                }
                else {
                    showMessage(data.Message, 'red');
                }
            }).fail(function(){
                showMessage("Server Error !!! Please Try After Some Time....", 'red');
            });
        }
        else{
            showMessage("Invalid Email Address Entered",'red');
        }
    }
    function forgotPassword(){
        $(".loader").show();
        var url = "api/admin_login.php";
        $.post(url, {"type": "forgotPassword"}, function (data) {
            $(".loader").hide();
            showMessage(data, 'green');
        }).fail(function(){
            $(".loader").hide();
            showMessage("Server Error !!! Please Try After Some Time....", 'red');
        });
    }
    function validateEmail(email) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    }
    function showCPDForm(){
        $(".modal-header").css({"display":"block"});
        $(".modal-title").html('Forgot Password');
        $(".modal-body").html("<div class='row'><div " +
        "class='col-md-3'></div><div class='col-md-6'><div class='form-group'><label>" +
        "New Password</label><input type='password' class='form-control' id='newPassword' /></div><div class='form-group'><label>" +
        "Confirm Password</label><input type='password' class='form-control' id='confirmNewPassword' /></div><input type='button' " +
        "class='btn btn-info pull-right' data-dismiss='modal' onclick=changeForgotPassword() value='Change Password'/>" +
        "</div></div><div class='col-md-3'></div>");
        $(".modal-footer").css({"display":"none"});
        $("#myModal").modal("show");
    }
    function changeForgotPassword() {
        var url = "api/admin_login.php";
        var newPassword = $("#newPassword").val();
        var confirmNewPassword = $("#confirmNewPassword").val();
        if (newPassword == confirmNewPassword) {
            setTimeout(function () {
                $.post(url, {
                    "type": "changeForgotPassword",
                    "new_password": newPassword
                }, function (data) {
                    var status = data.Status;
                    if (status == "Success") {
                        showMessage(data.Message, "green");
                    }
                    else {
                        showMessage(data.Message, "red");
                    }
                }).fail(function () {
                    showMessage("Server Error!!! Please Try After Some Time", "red")
                });
            }, 1000);
        }
        else {
            showMessage("New Password and Confirm Password Should Be Same", "red");
        }
    }
    $(document).ready(function() {
        if ($("#cpd").val() == "cpd") {
            showCPDForm();
        }
    });
</script>