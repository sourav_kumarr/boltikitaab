<?php
include('header.php');
include('api/Classes/CONNECT.php');
include('api/Classes/DISCOUNT.php');
include('api/Constants/DbConfig.php');
include('api/Constants/configuration.php');
$conn = new \Classes\CONNECT();
$disc = new \Classes\DISCOUNT();
?>
<!-- page content -->
<div class="right_col" role="main">
    <div class="row tile_count">
        <a href="users"><div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                <span class="count_top"><i class="fa fa-user"></i> Total Users</span>
                <div class="count" id="userCount"></div>
                <span class="count_bottom"><i class="green">Click </i>to Expand</span>
            </div></a>
        <a href="books"><div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                <span class="count_top"><i class="fa fa-volume-up"></i> Total Audio Books</span>
                <div class="count" id="booksCount"></div>
                <span class="count_bottom"><i class="green"></i> in All Categories</span>
            </div></a>
        <a href="index"><div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                <span class="count_top"><i class="fa fa-book"></i> Total Categories</span>
                <div class="count green" id="catCount"></div>
                <span class="count_bottom"><i class="green"></i> Click to Expand</span>
            </div></a>
        <a href="membership"><div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                <span class="count_top"><i class="fa fa-users"></i> MemberShip Types</span>
                <div class="count" id="membershipCount"></div>
                <span class="count_bottom"> Click to View</span>
            </div></a>
        <a href="discount"><div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                <span class="count_top"><i class="fa fa-cc-discover"></i> Discount Coupons</span>
                <div class="count" id="allCoupon"></div>
                <span class="count_bottom"><i class="green" id="activeCoupon"></i> is Still Active</span>
            </div></a>
        <a href="orders"><div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                <span class="count_top"><i class="fa fa-first-order"></i> Orders</span>
                <div class="count" id="orderCount"></div>
                <span class="count_bottom"><i class="green"></i>Click to Expand</span>
            </div></a>
    </div>
    <div class="">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>All Discount Coupons <small></small></h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <!--<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                            </li>-->
                            <li>
                                <button onclick="window.location='api/excelProcess.php?dataType=allCoupons'" class="btn btn-info btn-sm">Download Excel File</button>
                            </li>
                            <li>
                                <a class="btn btn-info" onclick='addNewDiscountCoupon()' style="color:white">
                                    <i class="fa fa-plus" ></i> Add New Discount Coupon
                                </a>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <p class="text-muted font-13 m-b-30">
                            Mantain the Discount Coupons and Add New Coupon Code
                        </p>
                        <table id="discountTable" class="table table-striped table-bordered">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Coupon Code</th>
                                <th>Coupon Value</th>
                                <th>Created Date </th>
                                <th>Expiry Date</th>
                                <th>Status</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $link = $conn->connect();
                            if ($link) {
                                $query = "select * from discount_coupon order by coupon_id DESC";
                                $result = mysqli_query($link, $query);
                                if ($result) {
                                    $num = mysqli_num_rows($result);
                                    if ($num > 0) {
                                        $j = 0;
                                        while ($couponData = mysqli_fetch_array($result)) {
                                            $j++;
                                            ?>
                                            <tr>
                                                <td data-title='#'><?php echo $j ?></td>
                                                <td data-title='Coupon Code'>
                                                    <!--<a href='udet?_=<?php /*echo $couponData['coupon_id'] */?>'>--><?php echo $couponData['coupon_code'] ?><!--</a>-->
                                                </td>
                                                <?php
                                                $amount = $couponData['coupon_value'];
                                                setlocale(LC_MONETARY, 'en_IN');
                                                $amount = money_format('%!i', $amount);
                                                ?>
                                                <td data-title='Coupon Value'>$<?php echo $amount ?></td>
                                                <td data-title='Generated On'><?php echo date("d M Y",$couponData['generated_on'])?></td>
                                                <td data-title='Expired On'><?php echo date("d M Y",$couponData['expired_on']) ?></td>
                                                <td class='buttonsTd' data-title='Coupon Status'>
                                                    <?php
                                                    $box = "";
                                                    if ($couponData['coupon_status'] == "1") {
                                                        $box = "checked";
                                                    }
                                                    if($couponData['generated_on']>strtotime(date("d M Y"))){
                                                        $box = "";
                                                        $disc->statusChange($couponData['coupon_id'],'0');
                                                    }
                                                    if(strtotime(date("d M Y"))>$couponData['expired_on']){
                                                        $box = "disabled";
                                                        $disc->statusChange($couponData['coupon_id'],'0');
                                                    }
                                                    ?>
                                                    <input data-onstyle='info' class='couponstatus' <?php echo $box ?>
                                                    data-toggle='toggle' value="<?php echo $couponData['coupon_status'] ?>"
                                                    id="<?php echo $couponData['coupon_id']?>" data-size='mini' data-style='ios'
                                                    type='checkbox' />
                                                </td>
                                                <td data-title='Action'>
                                                    <a href="addCoupon?id=<?php echo $couponData['coupon_id'] ?>"><i class='fa fa-edit' style='color:#31B0D5;cursor: pointer'></i></a>
                                                    <i class='fa fa-trash' onclick=deleteDiscountCoupon('<?php echo $couponData['coupon_id'] ?>') style='color:#D05E61;cursor: pointer'></i>
                                                </td>
                                            </tr>
                                            <?php
                                        }
                                    }
                                }
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /page content -->
<?php
include('footer.php');
?>

<script>
    $(document).ready(function () {
        $('#discountTable').DataTable({});
    });
</script>
