-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 26, 2017 at 07:54 AM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `stsmedtd_boltikitaab`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin_login`
--

CREATE TABLE `admin_login` (
  `admin_id` int(100) NOT NULL,
  `admin_name` varchar(100) NOT NULL,
  `admin_email` varchar(100) NOT NULL,
  `admin_password` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin_login`
--

INSERT INTO `admin_login` (`admin_id`, `admin_name`, `admin_email`, `admin_password`) VALUES
(1, 'Admin', 'admin@gmail.com', 'e10adc3949ba59abbe56e057f20f883e');

-- --------------------------------------------------------

--
-- Table structure for table `books`
--

CREATE TABLE `books` (
  `book_id` int(100) NOT NULL,
  `cat_id` int(100) NOT NULL,
  `book_name` varchar(100) NOT NULL,
  `book_desc` text NOT NULL,
  `book_author` varchar(100) NOT NULL,
  `book_narrator` varchar(100) NOT NULL,
  `play_time` varchar(100) NOT NULL,
  `list_price` float NOT NULL,
  `discount_id` varchar(100) NOT NULL,
  `book_status` enum('0','1') NOT NULL,
  `audio_file` varchar(100) NOT NULL,
  `short_audio_file` varchar(1000) NOT NULL,
  `front_look` varchar(100) NOT NULL,
  `average_rate` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `books`
--

INSERT INTO `books` (`book_id`, `cat_id`, `book_name`, `book_desc`, `book_author`, `book_narrator`, `play_time`, `list_price`, `discount_id`, `book_status`, `audio_file`, `short_audio_file`, `front_look`, `average_rate`) VALUES
(6, 14, 'New Test', 'Helloooooooo', 'Harry', 'RaviKant', '00:04:04', 120, '5', '1', 'File529541.mp3', 'File971710.mp3', 'File402526.jpg', '4.5'),
(7, 15, 'New Test', 'Helloooooooo', 'Harry', 'RaviKant', '00:04:04', 120, '5', '1', 'File529541.mp3', 'File971710.mp3', 'File402526.jpg', '3.5'),
(8, 14, 'New Test', 'Helloooooooo', 'Harry', 'RaviKant', '00:04:04', 120, '5', '1', 'File529541.mp3', 'File971710.mp3', 'File402526.jpg', '5');

-- --------------------------------------------------------

--
-- Table structure for table `book_rating`
--

CREATE TABLE `book_rating` (
  `rating_id` int(100) NOT NULL,
  `book_id` varchar(100) NOT NULL,
  `user_rate` varchar(100) NOT NULL,
  `user_review` text NOT NULL,
  `user_id` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `book_rating`
--

INSERT INTO `book_rating` (`rating_id`, `book_id`, `user_rate`, `user_review`, `user_id`) VALUES
(1, '7', '3.5', 'Awesome Book I love to Download This', '9'),
(2, '6', '4.0', 'Awesome Book I love to Download This', '9');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `cat_id` int(100) NOT NULL,
  `cat_name` varchar(100) NOT NULL,
  `cat_image` varchar(100) NOT NULL,
  `added_on` varchar(100) NOT NULL,
  `cat_status` enum('0','1') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`cat_id`, `cat_name`, `cat_image`, `added_on`, `cat_status`) VALUES
(14, 'Kapil', 'File10009.png', '21 Mar 2017 12:54:22 PM', '1'),
(15, 'Sunil', 'File658935.jpg', '23 Mar 2017 04:00:05 AM', '1');

-- --------------------------------------------------------

--
-- Table structure for table `discount_coupon`
--

CREATE TABLE `discount_coupon` (
  `coupon_id` int(100) NOT NULL,
  `coupon_code` varchar(100) NOT NULL,
  `coupon_value` text NOT NULL,
  `generated_on` varchar(100) NOT NULL,
  `expired_on` varchar(100) NOT NULL,
  `coupon_status` enum('0','1') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `discount_coupon`
--

INSERT INTO `discount_coupon` (`coupon_id`, `coupon_code`, `coupon_value`, `generated_on`, `expired_on`, `coupon_status`) VALUES
(1, 'ABCD1020', '100.0', '1490137200', '1493330400', '1'),
(3, 'BKB33', '33.10', '1490137200', '1492552800', '1'),
(5, 'Code50', '50.00', '1491861600', '1491948000', '0');

-- --------------------------------------------------------

--
-- Table structure for table `membership`
--

CREATE TABLE `membership` (
  `plan_id` int(100) NOT NULL,
  `plan_name` varchar(100) NOT NULL,
  `plan_price` varchar(100) NOT NULL,
  `renewal_type` varchar(100) NOT NULL,
  `plan_status` enum('0','1') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `membership`
--

INSERT INTO `membership` (`plan_id`, `plan_name`, `plan_price`, `renewal_type`, `plan_status`) VALUES
(5, 'Basic', '0', 'all', '1'),
(6, 'Premium', '120', 'all', '1'),
(7, 'newtest', '57', 'all', '1'),
(8, 'sda', '120.88', 'all', '1');

-- --------------------------------------------------------

--
-- Table structure for table `orderdetail`
--

CREATE TABLE `orderdetail` (
  `detail_id` int(100) NOT NULL,
  `order_id` int(100) NOT NULL,
  `item_id` int(100) NOT NULL,
  `item_price` int(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `orderdetail`
--

INSERT INTO `orderdetail` (`detail_id`, `order_id`, `item_id`, `item_price`) VALUES
(1, 1, 1, 120),
(2, 2, 4, 120),
(3, 2, 2, 95);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `order_id` int(100) NOT NULL,
  `order_number` varchar(100) NOT NULL,
  `transaction_id` varchar(100) NOT NULL,
  `amount` varchar(100) NOT NULL,
  `payment_status` varchar(100) NOT NULL,
  `order_dated` varchar(100) NOT NULL,
  `order_userId` varchar(100) NOT NULL,
  `order_status` varchar(100) NOT NULL,
  `refund_generated` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`order_id`, `order_number`, `transaction_id`, `amount`, `payment_status`, `order_dated`, `order_userId`, `order_status`, `refund_generated`) VALUES
(1, '5641446', '4se656f4ewfw5e1fs5e1651e6f', '120', 'Success', '1488733403', '9', 'Success', 'No'),
(2, '546545643', 'sdss561g65s1g5ds6f5ds4f54sd6fsd', '215', 'Failed', '1491152603', '9', 'Cancelled', 'No'),
(3, '21656481', '4sfe654f65s465f4se65f4w6w5e', '140.52', 'Success', '1491152603', '9', 'Cancelled', 'Yes');

-- --------------------------------------------------------

--
-- Table structure for table `recentviewed`
--

CREATE TABLE `recentviewed` (
  `_id` int(100) NOT NULL,
  `book_id` varchar(100) NOT NULL,
  `last_seen` varchar(100) NOT NULL,
  `user_id` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `recentviewed`
--

INSERT INTO `recentviewed` (`_id`, `book_id`, `last_seen`, `user_id`) VALUES
(1, '6', '1498206757', '19'),
(2, '1', '1498222497', '2'),
(3, '7', '1498454740', '874FA7FD-879F-420C-97ED-7CDECD708C54'),
(4, '8', '1498453347', '874FA7FD-879F-420C-97ED-7CDECD708C54');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` int(100) NOT NULL,
  `user_name` varchar(100) NOT NULL,
  `user_email` varchar(100) NOT NULL,
  `user_contact` varchar(100) NOT NULL,
  `active_plan` varchar(100) NOT NULL,
  `activation_date` varchar(100) NOT NULL,
  `plan_expiry_date` varchar(100) NOT NULL,
  `user_status` enum('0','1') NOT NULL,
  `user_token` text NOT NULL,
  `user_address` text NOT NULL,
  `register_source` varchar(100) NOT NULL,
  `renewal_type` varchar(100) NOT NULL,
  `auto_renewal` varchar(100) NOT NULL,
  `email_verified` enum('Yes','No') NOT NULL,
  `user_profile` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `user_name`, `user_email`, `user_contact`, `active_plan`, `activation_date`, `plan_expiry_date`, `user_status`, `user_token`, `user_address`, `register_source`, `renewal_type`, `auto_renewal`, `email_verified`, `user_profile`) VALUES
(19, 'Sunil Bhatia', 'sbsunilbhatia9@gmail.com', '8872292478', '6', '1498203568', '1529653168', '1', '05e328d0616037c76a7f329cadcaf50c071e2be3422792fd0bc1f4d62eb80f', 'majitha', 'Facebook', 'Yearly', 'Yes', 'No', 'images url');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin_login`
--
ALTER TABLE `admin_login`
  ADD PRIMARY KEY (`admin_id`);

--
-- Indexes for table `books`
--
ALTER TABLE `books`
  ADD PRIMARY KEY (`book_id`);

--
-- Indexes for table `book_rating`
--
ALTER TABLE `book_rating`
  ADD PRIMARY KEY (`rating_id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`cat_id`);

--
-- Indexes for table `discount_coupon`
--
ALTER TABLE `discount_coupon`
  ADD PRIMARY KEY (`coupon_id`);

--
-- Indexes for table `membership`
--
ALTER TABLE `membership`
  ADD PRIMARY KEY (`plan_id`);

--
-- Indexes for table `orderdetail`
--
ALTER TABLE `orderdetail`
  ADD PRIMARY KEY (`detail_id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`order_id`);

--
-- Indexes for table `recentviewed`
--
ALTER TABLE `recentviewed`
  ADD PRIMARY KEY (`_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin_login`
--
ALTER TABLE `admin_login`
  MODIFY `admin_id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `books`
--
ALTER TABLE `books`
  MODIFY `book_id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `book_rating`
--
ALTER TABLE `book_rating`
  MODIFY `rating_id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `cat_id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `discount_coupon`
--
ALTER TABLE `discount_coupon`
  MODIFY `coupon_id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `membership`
--
ALTER TABLE `membership`
  MODIFY `plan_id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `orderdetail`
--
ALTER TABLE `orderdetail`
  MODIFY `detail_id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `order_id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `recentviewed`
--
ALTER TABLE `recentviewed`
  MODIFY `_id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
